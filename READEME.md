#Titans
*0.1.44-dev*

##Overview

________________

Titans is a plugin designed for <a href="http://forums.pigcraft.net/index.php">Pigcraft QC.</a> Its primary goal is to allow players to transform themselves into super-powerful beings unsurprisingly called Titans. These Titans possess a variety of 'skills', abilities which can be used through the */titan skill [skill name]* command.


##Installation

_______________

Titans is a simple plugin to install; simply download the latest version from <a href="about:blank">here</a> and drop the Jar File into your plugins folder. Restart your server if necessary.

###What about Configuration?

Most configuration is done with the individual Titans themselves; as a result of this, the actual Titans core framework itself requires no configuration on your part :)

##Commands

_______________

**/titan on [titan name]** : Turns the user into the specified Titan. Will call the specified Titan's onEntrance() method.

**/titan off** : If the command sender is a Titan, this command will return them to their normal player state.

**/titan skill [skill name]** : If the command sender is a Titan, this command runs the specified TitanSkill.

**/titan help** : Displays help for the plugin.

**/titan lore [titan]** : Displays the description for the specified Titan.

**/titan list** : Lists all loaded Titans and their controllers, if they have any.

**/titan info [titan]** : More detailed version of /titan lore. Displays the specified Titan's skills as well as its description.


##Permissions

________________

**titans.*** : Grants all permissions

**titans.use** : Grants access to the /titan command and all it's subcommands


Currently more permissions are in development.

##Credits

_______________

**EvilConifer** : Project Lead, Programming

**Devojha** : Programming

**Catsof**: Programming

**NazzyDragon, TB, HappyPikachu**: Designers