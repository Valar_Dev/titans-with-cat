package net.pigcraft.code.titans.builtins.smdp;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TargetedTitanSkill;


public final class LightningSkill extends TargetedTitanSkill
{
	static int ShotBallID = 0;
    public LightningSkill(final Titan titan)
    {
        super("Lightning", titan, 20, new Cooldown(5));
        this.setAltNames(new String[]{"lt", "light"});
        this.setDescription("Use typical thunderbolt attack");
    }

    @Override
    public void use()
    {
    	if(Math.random() > .3)
    	{
    		this.getTarget().getWorld().strikeLightning(this.getTarget().getLocation());
    	}
    	else
    		this.getTitan().broadcastMsg("Lightning skill failed.");
    }
}