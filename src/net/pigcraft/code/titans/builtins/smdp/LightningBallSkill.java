package net.pigcraft.code.titans.builtins.smdp;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.entity.Snowball;

public final class LightningBallSkill extends TitanSkill
{
	static int ShotBallID = 0;
    public LightningBallSkill(final Titan titan)
    {
        super("ElectroBall", titan, new Cooldown(5));
        this.setAltNames(new String[]{"eb", "ball"});
        this.setDescription("Shoots a ball of electricity at foe");
    }

    @Override
    public void use()
    {
        final Snowball lightningBall = this.getTitan().getController().getWorld().spawn(this.getTitan().getController().getLocation(), Snowball.class);
        lightningBall.setShooter(this.getTitan().getController());
        lightningBall.setVelocity(this.getTitan().getController().getLocation().getDirection().multiply(1.5));
        LightningBallSkill.ShotBallID = lightningBall.getEntityId();
    }
}
