package net.pigcraft.code.titans.builtins.ixen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.pigcraft.code.titans.Titans;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public final class AttackListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
    	if (e.getDamager() instanceof Player) {
    		if (Titans.plugin.isPlayerATitan((Player) e.getDamager())) {
    			if (Titans.plugin.getTitanAssignedToPlayer((Player) e.getDamager()).getName().equalsIgnoreCase("Ixen'Gix")) {
    				if (((IxenGix) Titans.plugin.getTitanAssignedToPlayer((Player) e.getDamager())).isReadyToBite()) {
    					((Bite) Titans.plugin.getTitanAssignedToPlayer((Player) e.getDamager()).getSkill("bite")).onAttack(e.getEntity());
    				}
    				
    				if (((IxenGix) Titans.plugin.getTitanAssignedToPlayer((Player) e.getDamager())).isReadyToRake()) {
    					ArrayList<Player> players = playersDistanceSorted(e.getDamager().getLocation(), 13, 5);
    					/*for (Player p : e.getDamager().getWorld().getPlayers()) {
    						if(players.size() < 5) {
    							if(p.getLocation().distanceSquared(e.getDamager().getLocation()) <=4) {
    								players.add(p);
    							}
    						}
    					} Cats Player senser. Doesn't seem right, since its in the world */
    					
    					((Rake) Titans.plugin.getTitanAssignedToPlayer((Player) e.getDamager()).getSkill("rake")).onAttack(players);
    				}
    				
    			}
    		}
    	}
    }
    
    public static ArrayList<Player> playersDistanceSorted(Location loc,
    		double radius, int howMany) {
    		Map<Double, Player> Players = new HashMap<Double, Player>();
    		ArrayList<Player> players = new ArrayList<Player>();
    		 
    		double i1 = loc.getX();
    		double j1 = loc.getY();
    		double k1 = loc.getZ();
    		 
    		for (Player player : Bukkit.getOnlinePlayers()) {
    		 
    		if (player.getWorld().equals(loc.getWorld())) {
    		 
    		double i2 = player.getLocation().getX();
    		double j2 = player.getLocation().getY();
    		double k2 = player.getLocation().getZ();
    		 
    		double ad = Math.sqrt((i2 - i1)*(i2 - i1) + (j2 - j1)*(j2 - j1) + (k2 - k1)*(k2 - k1));
    		 
    		if (ad < radius) {
    		Players.put(ad, player);
    		}
    		 
    		}
    		 
    		}
    		if(Players.size() > howMany)
    		{
    		ArrayList<Double> Radii = new ArrayList<Double>();
    		for(double key : Players.keySet())
    			Radii.add(key);
    		Collections.sort(Radii);
    		for(int i = 0; i < howMany; i++)
    			players.add(Players.get(Radii.get(i)));
    		}
    		else
    			for(double d : Players.keySet())
    				players.add(Players.get(d));
    		return players;
    		 
    	}
    

}
