package net.pigcraft.code.titans.builtins.ixen;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.Broadcast;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

import org.bukkit.ChatColor;
import org.bukkit.block.Biome;


public final class DragonShiftSkill extends DurationTitanSkill
{
	//I NEED TO SWITCH THIS TO MAGIC SPELLS BEFORE RELEASE
	//Should be really easy though. 
    private final Titan ixen;

    private Duration duration;

    private float prevArmor;
    private float prevAttackPower;
    static float prevWalkSpeed;

    public DragonShiftSkill(final Titan titan)
    {
        super("shift", titan, new Cooldown(200));

        this.setDescription("Shifts from humanoid to Dragon state.");
        this.ixen = Titans.plugin.getTitan("Ixen'gix");
        this.duration = new Duration(90L, this);
    }

    @Override
    public void use()
    {

    	Broadcast.Broadcastmsg(ChatColor.DARK_RED +"Feel the fury within! You've angered me for the last time!", 75, this.getTitan());
        if (this.getTitan().getController().getLocation().getBlock().getBiome() == Biome.HELL)
        {
            this.duration = new Duration(600L, this);
        }

        if (!IxenGix.isInDragonState())
        {
            this.getTitan().broadcastMsg(ChatColor.GOLD + "*Feel the fury within! You've angered me for the last time!");
            
            this.prevArmor = IxenGix.armorModifier;
            this.prevAttackPower = IxenGix.attackPowerModifier;
            DragonShiftSkill.prevWalkSpeed = this.ixen.getController().getWalkSpeed();
            if(FlameWakeSprintSkill.getRunning())
            {
            	DragonShiftSkill.prevWalkSpeed = FlameWakeSprintSkill.prevMoveSpeed;
            }

            IxenGix.armorModifier = prevArmor /= 2.5F;
            IxenGix.attackPowerModifier = prevAttackPower *= 1.5F;
            this.ixen.getController().setWalkSpeed(prevWalkSpeed * 1.5F);

            this.getTitan().getController().setAllowFlight(true);

            IxenGix.setInDragonState(true);

            this.duration.startDuration();
        }
        else
        {
            this.getTitan().getController().sendMessage(ChatColor.RED + "You're already in Dragon state!");
        }

    }

    @Override
    public void DurationDone()
    {
        
        IxenGix.armorModifier = this.prevArmor;
        IxenGix.attackPowerModifier = this.prevAttackPower;

        this.ixen.getController().setWalkSpeed(DragonShiftSkill.prevWalkSpeed);

        this.getTitan().getController().setAllowFlight(false);

        IxenGix.setInDragonState(false);
    }
}
