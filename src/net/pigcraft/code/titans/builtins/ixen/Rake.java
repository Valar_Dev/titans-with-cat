package net.pigcraft.code.titans.builtins.ixen;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Broadcast;
import net.pigcraft.code.titans.skills.TitanSkill;

public final class Rake extends TitanSkill {

	public Rake(final Titan titan) {
		super("rake", titan, new Cooldown(1));
		this.setAltNames(new String[]{"rk"});
		this.setDescription("Rakes the melee target(s) [AoE]. Powerful knockback and bleed effect, causes blindness.");
	}

	@Override
	public void use() {
		Broadcast.Broadcastmsg("*"+ChatColor.DARK_RED + "Lady Ixen'gix readies her claws!", 11, this.getTitan());
		((IxenGix) this.getTitan()).readyRake();
		((IxenGix) this.getTitan()).doneBite();
		this.getTitan().getController().sendMessage("Has readied Rake successfully.");
	}
	
	public void onAttack(final ArrayList<Player> targets) {
		// Testing purposes
		/*this.getTitan().getController().sendMessage("Has attacked Rake successfully.");
		((Player) target).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 3, 1));
		((IxenGix) this.getTitan()).doneRake();*/
		final Player titan = this.getTitan().getController();
		for(int i = 0; i <= 4; i++)
		{
			final int cur = i;
			Bukkit.getScheduler().scheduleSyncDelayedTask(Titans.plugin,  new Runnable()
		      {
		        @Override
		        public void run()
		        {
		        	Player p = targets.get(cur);
		        	Vector NewVector = p.getLocation().toVector().subtract(titan.getLocation().toVector());
					NewVector = NewVector.multiply(1.1);
					NewVector = NewVector.add(new Vector(0, 1, 0));
					titan.setVelocity(NewVector);
					//Do damage
					ItemStack item = new ItemStack(Material.IRON_SWORD);
					item.setDurability((short) 1);
					item.addEnchantment(Enchantment.KNOCKBACK, 2);
					
					titan.setItemInHand(item);
					p.damage(3, titan);
					p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 2, 3));
		        }
		      }, 25L * i);
		}
	}


}
