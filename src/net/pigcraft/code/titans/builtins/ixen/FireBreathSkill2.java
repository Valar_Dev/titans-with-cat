package net.pigcraft.code.titans.builtins.ixen;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.Broadcast;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Biome;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public final class FireBreathSkill2 extends TitanSkill
{
    private static final float SPEED = 2.5F;
    private static final short MAX_FIREBALLS = 15;
    public static int cur = 0;
    public static BukkitTask task = null;
    private final List<Integer> fireballs;
    public Cooldown HellCool = new Cooldown(10);
    public Cooldown RegCool = new Cooldown(20);

    public FireBreathSkill2(final Titan titan)
    {
        super("fireBreath", titan, new Cooldown(1));
        this.setAltNames(new String[]{"fb"});

        this.setDescription("Spits out a cone of fire in front of the caster, causing \"Wither\" to those struck in the blast.");

        this.fireballs = new ArrayList<Integer>(MAX_FIREBALLS);
    }

    @Override
    public void use()
    {
    	if(HellCool.done() && RegCool.done()){
    	Broadcast.Broadcastmsg("*" + ChatColor.DARK_RED + "Lady Ixen'gix takes a deep breath...", 40, this.getTitan());
        final Player p = this.getTitan().getController();
        Biome biome = p.getWorld().getBiome(p.getLocation().getBlockX(), p.getLocation().getBlockZ());
        if(biome == Biome.HELL)
        {
        	HellCool.start();
        }
        else
        	RegCool.start();
        task = Bukkit.getScheduler().runTaskTimer(Titans.plugin, new Runnable() 
        {
        	@Override
	        	public void run() 
        	{
        		 Vector direction = p.getEyeLocation().getDirection().multiply(SPEED);
        		 Projectile projectile = (Projectile)p.getWorld().spawn(p.getEyeLocation().add(direction.getX(), direction.getY(), direction.getZ()), type());
                 projectile.setShooter(p);
                 projectile.setVelocity(direction);
                 if(projectile instanceof Fireball)
                	 fireballs.add(projectile.getEntityId());
                 if(cur == 15)
                 {
                	 cur = 0;
                     task.cancel();
                 }
                 else
                	 cur++;
	        }
        }, 1, 6);
    	}
    	else{
    		if(HellCool.timeLeft() > RegCool.timeLeft())
    			this.getTitan().getController().sendMessage(ChatColor.GRAY + "Error, " + ChatColor.DARK_RED + "Fire Breath" +
                        ChatColor.GRAY + " is on cooldown for "
                        + ChatColor.RED + HellCool.timeLeft() +ChatColor.GRAY + " more seconds");
    		else
    			this.getTitan().getController().sendMessage(ChatColor.GRAY + "Error, " + ChatColor.DARK_RED + "Fire Breath" +
                        ChatColor.GRAY + " is on cooldown for "
                        + ChatColor.RED + RegCool.timeLeft() +ChatColor.GRAY + " more seconds");
    	}
    }
    
    @EventHandler
    public final void onProjectileHit(EntityDamageByEntityEvent event)
    {
        if (event.getDamager() instanceof Fireball || event.getDamager() instanceof LargeFireball)
        {
            if (this.fireballs.contains(event.getDamager().getEntityId()))
            {
                if (event.getEntityType() == EntityType.PLAYER)
                {
                    Player target = (Player)event.getEntity();
                    target.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 120, 2));

                    this.fireballs.remove((Integer)event.getDamager().getEntityId());
                    event.setDamage(event.getDamage() + 5.5);
                }
            }
        }
    }
    
    public Class<? extends Entity> type()
    {
    	Double rand = Math.random();
    	if(rand >.7)
    		return LargeFireball.class;
    	
    	else
    		return Fireball.class;
    	
    }

	
}