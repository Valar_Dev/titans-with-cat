package net.pigcraft.code.titans.builtins.ixen;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Broadcast;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

public final class Bite extends TitanSkill {

	public Bite(final Titan titan) {
		super("bite", titan, new Cooldown(1));
		this.setAltNames(new String[]{"bt"});
		this.setDescription("Bites a single melee target, causing bleeding and weakness.");
	}

	@Override
	public void use() {
		Broadcast.Broadcastmsg("*" + ChatColor.DARK_RED + "Lady Ixen'gix readies her fangs!", 5, this.getTitan());
		((IxenGix) this.getTitan()).readyBite();
		((IxenGix) this.getTitan()).doneRake();
		this.getTitan().getController().sendMessage("Has readied Bite successfully.");
	}
	
	public void onAttack(Entity target) {
		// Testing purposes
		if (target instanceof Player) {
			((Player) target).addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 2, 1));
		}
		this.getTitan().getController().sendMessage("Has attacked Bite successfully.");
		((IxenGix) this.getTitan()).doneBite();
	}

}
