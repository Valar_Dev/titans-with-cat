package net.pigcraft.code.titans.builtins.ixen;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Random;

public final class IxenGix extends Titan implements Listener
{

    protected static final Random RANDOM = new Random(1337);

    private String prevDisplayName;
    private double prevMaxHealth;
    private double prevHealth;
    private float prevWalkSpeed;
    
    private boolean rakeReady = false;
    private boolean biteReady = false;

    private static boolean dragonState;

    protected static float armorModifier = 0.15F;
    protected static float attackPowerModifier = 2F;

    public IxenGix()
    {
        super
        (
            Titans.plugin,
            "Ixen'gix",
            "The ground quakes as the Flame Gate opens; a hellish beast emerges!",
            "A calm falls over the land as the Flame Queen phases back to her own dimension.",
            ChatColor.DARK_RED + "Ixen'gix: " + ChatColor.GOLD + "<message>"
        );

        this.setDescription
                (
                        "I'm sure this is a blast from the past for most people here. Queen of the Flame Gate; guardian of the" +
                                " entrance to the realm of the dead, hell itself. Lady Ixen'gix protects the last standing nether portal" +
                                " from those who wish to enter it, but in the years past since her last rising (before being banished" +
                                " back to the Nether) the Flame Gate has been sealed, requiring great magical power to open. Only she" +
                                " can open it, despite the attempt to seal it permanently. Content with the seal on the gate that " +
                                "prevents mortals from entering, she roams the land freely, seeking to eliminate all from it, and " +
                                "claim another realm in the name of Marfedelom, late ruler of the Nether."
                );

        final ArrayList<EntityType> mobIgnoreList = new ArrayList<EntityType>(7);
        mobIgnoreList.add(EntityType.WITHER);
        mobIgnoreList.add(EntityType.ENDER_DRAGON);
        mobIgnoreList.add(EntityType.MAGMA_CUBE);
        mobIgnoreList.add(EntityType.GHAST);
        mobIgnoreList.add(EntityType.BLAZE);
        mobIgnoreList.add(EntityType.PIG_ZOMBIE);
        mobIgnoreList.add(EntityType.SKELETON);
        this.SetMobProtectionList(mobIgnoreList);

        final ArrayList<DamageCause> immunityList = new ArrayList<DamageCause>(4);
        immunityList.add(DamageCause.FALL);
        immunityList.add(DamageCause.LAVA);
        immunityList.add(DamageCause.FIRE);
        immunityList.add(DamageCause.FIRE_TICK);
        immunityList.add(DamageCause.WITHER);
        this.SetWorldProtectionList(immunityList);
    }

    @Override
    public void onEntrance()
    {
        for (final Player player : this.getController().getWorld().getPlayers())
        {
            this.getController().getWorld().playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 1, 0);
        }

        this.prevDisplayName = this.getController().getDisplayName();
        this.getController().setDisplayName(ChatColor.DARK_RED + "Lady Ixen'gix");

        this.prevMaxHealth = this.getController().getMaxHealth();
        this.prevHealth = this.getController().getHealth();
        this.getController().setMaxHealth(250D);
        this.getController().setHealth(250D);

        this.getController().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2), true);
        this.getController().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1), true);

        this.prevWalkSpeed = this.getController().getWalkSpeed();
        this.getController().setWalkSpeed(this.getController().getWalkSpeed() * 2.5F);
    }

    @Override
    public void onExit()
    {
        for (final Player player : this.getController().getWorld().getPlayers())
        {
            this.getController().getWorld().playSound(player.getLocation(), Sound.PORTAL_TRAVEL, 1, 0);
        }

        this.getController().setDisplayName(this.prevDisplayName);

        this.getController().setMaxHealth(this.prevMaxHealth);
        this.getController().setHealth(this.prevHealth);

        this.getController().removePotionEffect(PotionEffectType.JUMP);
        this.getController().removePotionEffect(PotionEffectType.NIGHT_VISION);

        this.getController().setWalkSpeed(this.prevWalkSpeed);
    }

    @Override
    public void onLoad()
    {
        this.addSkill(new FireBreathSkill2(this));
        this.addSkill(new FlameWakeSprintSkill(this));
        this.addSkill(new DragonShiftSkill(this));
        this.addSkill(new Bite(this));
        this.addSkill(new Rake(this));

        this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin.getServer().getPluginManager().registerEvents(this.getSkill("fireBreath"), plugin);
        this.plugin.getServer().getPluginManager().registerEvents(this.getSkill("flameWakeSprint"), plugin);
        this.plugin.getServer().getPluginManager().registerEvents(this.getSkill("bite"), plugin);
        this.plugin.getServer().getPluginManager().registerEvents(this.getSkill("rake"), plugin);
        

    	Titans.plugin.getServer().getPluginManager().registerEvents(new AttackListener(), Titans.plugin);
    }

    @Override
    public void onUnload()
    {
        this.clearSkills();
    }


    protected static boolean isInDragonState()
    {
        return dragonState;
    }
    
    protected boolean isReadyToBite() {
    	return biteReady;
    }
    
    protected boolean isReadyToRake() {
    	return rakeReady;
    }

    protected void readyBite() {
    	biteReady = true;
    }
    protected void doneBite() {
    	biteReady = false;
    }
    protected void readyRake() {
    	rakeReady = true;
    }
    protected void doneRake() {
    	rakeReady = false;
    }

    protected static void setInDragonState(final boolean flag)
    {
        dragonState = flag;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public final void onHungerUpdate(FoodLevelChangeEvent event)
    {
        if (event.getEntity() instanceof Player)
        {
            if (Titans.plugin.isPlayerATitan((Player)event.getEntity()))
            {
                if (Titans.plugin.getTitanAssignedToPlayer((Player)event.getEntity()).getName().equals("Ixen'gix"))
                {
                    int i = RANDOM.nextInt((10 - 1) + 1);

                    int postChangeHunger = event.getFoodLevel();
                    int prevChangeHunger = ((Player) event.getEntity()).getFoodLevel();

                    if (i <= 8 && postChangeHunger < prevChangeHunger )
                    {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public final void onDamageReceived(EntityDamageEvent event)
    {
        if (event.getEntity() instanceof Player)
        {
            if (Titans.plugin.isPlayerATitan((Player)event.getEntity()))
            {
                if (Titans.plugin.getTitanAssignedToPlayer((Player)event.getEntity()).getName().equals("Ixen'gix"))
                {
                    double dmg = event.getDamage();

                    if (event.getCause().equals(DamageCause.POISON))
                    {
                        event.setDamage(dmg *= 1.5D);
                    }
                    else
                    {
                        event.setDamage(dmg *= armorModifier);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public final void onDamageDealt(EntityDamageByEntityEvent event)
    {
        if (event.getDamager() instanceof Player)
        {
            if (Titans.plugin.isPlayerATitan((Player)event.getDamager()))
            {
                if (Titans.plugin.getTitanAssignedToPlayer((Player)event.getDamager()).getName().equals("Ixen'gix"))
                {
                    if (((Player) event.getDamager()).getItemInHand().getType() == Material.AIR)
                    {
                        double dmg = event.getDamage();
                        event.setDamage(dmg += attackPowerModifier);
                    }
                }
            }
        }
    }

}
