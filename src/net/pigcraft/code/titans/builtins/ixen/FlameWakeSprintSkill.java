package net.pigcraft.code.titans.builtins.ixen;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public final class FlameWakeSprintSkill extends DurationTitanSkill
{

    private Duration skillDuration;
    private Duration hellDuration;
    private Cooldown hellCooldown = new Cooldown(645);
    private Cooldown skillCooldown = new Cooldown(52);
    static float prevMoveSpeed;
    private static boolean running;
    Player p;

    public FlameWakeSprintSkill(final Titan titan)
    {
        super("flameWakeSprint", titan, new Cooldown(1));
        this.setAltNames(new String[]{"sprint", "fs"});
        this.setDescription("Caster gains increased speed for set duration, leaves fire in their wake.");
        this.skillDuration = new Duration(7, this);
        this.hellDuration = new Duration(600, this);
    }

    @Override
    public void use()
    {
    	if(skillCooldown.done() && hellCooldown.done()){
	        this.getTitan().getController().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 12000, 5), true);
	        FlameWakeSprintSkill.prevMoveSpeed = this.getTitan().getController().getWalkSpeed();
	        if(IxenGix.isInDragonState())
	        	FlameWakeSprintSkill.prevMoveSpeed = DragonShiftSkill.prevWalkSpeed;
	        this.getTitan().getController().setWalkSpeed(FlameWakeSprintSkill.prevMoveSpeed * 1.5F);
	        FlameWakeSprintSkill.running = true;
	        p = this.getTitan().getController();
	        Biome biome = this.getTitan().getController().getWorld().getBiome(this.getTitan().getController().getLocation().getBlockX(), this.getTitan().getController().getLocation().getBlockZ());
	        if(biome == Biome.HELL){
	        	this.hellDuration.startDuration();
	        	this.hellCooldown.start();
	        }
	        else
	        {
	        	this.skillDuration.startDuration();
	        	this.skillCooldown.start();
	        }
    	}
    	else{
    		if(hellCooldown.timeLeft() > skillCooldown.timeLeft())
    			this.getTitan().getController().sendMessage(ChatColor.GRAY + "Error, " + ChatColor.DARK_RED + "Flame Wake Sprint" +
                        ChatColor.GRAY + " is on cooldown for "
                        + ChatColor.RED + hellCooldown.timeLeft() +ChatColor.GRAY + " more seconds");
    		else
    			this.getTitan().getController().sendMessage(ChatColor.GRAY + "Error, " + ChatColor.DARK_RED + "Flame Wake Sprint" +
                        ChatColor.GRAY + " is on cooldown for "
                        + ChatColor.RED + skillCooldown.timeLeft() +ChatColor.GRAY + " more seconds");
    	}
    }

    @Override
    public void DurationDone()
    {
    	p.removePotionEffect(PotionEffectType.JUMP);
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2), true);
        FlameWakeSprintSkill.running = false;
        if(IxenGix.isInDragonState())
        	return;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        if(event.isCancelled() || event.getFrom().getBlock().getLocation() == event.getFrom().getBlock().getLocation())
        {
            return;
        }

        if (Titans.plugin.isPlayerATitan(event.getPlayer()))
        {
            if (Titans.plugin.getTitanAssignedToPlayer(event.getPlayer()).getName().equals("Ixen'gix"))
            {
                if (FlameWakeSprintSkill.running)
                {
                	if(event.getFrom().getBlock().getType() == Material.FIRE)
                		return;
                    event.getFrom().getBlock().setType(Material.FIRE);
                    final Block bloc = event.getFrom().getBlock();
                    Bukkit.getScheduler().runTaskLater(Titans.plugin, new Runnable() 
                    {
                    	@Override
            	        	public void run() 
                    	{
                    		bloc.setType(Material.AIR);
            	        }
                    }, 100);
                }
            }
        }
    }
    
    public static boolean getRunning(){
    	return running;
    }

}
