package net.pigcraft.code.titans.builtins.avistlo;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

public final class LighterThanAirSkill extends DurationTitanSkill
{
	Player titan;
    Duration duration = new Duration(60, this);
    private float prevFlySpeed;
    public LighterThanAirSkill(final Titan titan)
    {
        super("Fly", titan, new Cooldown(90));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Lighter than Air"
        + "\n          Avistlo can fly for a period of time.");
        String[] Aliases = {"Air", "fl", "fyl", "ari"};
        this.setAltNames(Aliases);
    }
    
    
    @Override
    public void use()
    {
    	titan  = this.getTitan().getController();
    	this.prevFlySpeed = titan.getFlySpeed();
    	titan.setAllowFlight(true);
    	titan.setFlying(true);
    	titan.setFlySpeed(.175f);
    	duration.startDuration();
    }

	@Override
	public void DurationDone() {
		titan.setFlySpeed(this.prevFlySpeed);
		titan.setFlying(false);
		titan.setAllowFlight(false);
	}
}
