package net.pigcraft.code.titans.builtins.avistlo;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.PassiveTitanSkill;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public final class AvistloIncomingDamageListener implements Listener
{
    @EventHandler
    public void onDamager(EntityDamageByEntityEvent e){
    	if(e.getEntity() instanceof Player)
    	{
    		//I do multiple if statements for a sifting sort of thing, making it faster to run for everyone else.
            if(Titans.plugin.isPlayerATitan((Player)e.getEntity()))
            {
        		if(Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity()).getName().equalsIgnoreCase("Avistlo"))
        		{
        			if(e.getDamager().getType() != EntityType.PLAYER && e.getDamager().getType() != EntityType.ARROW)
        			{
        				return;
        			}
        			else if(e.getDamager().getType() == EntityType.PLAYER || e.getDamager().getType() == EntityType.ARROW)
        			{

        				//This sets iron level protection to Avistlo.
        				Double receivedDamage = e.getDamage();
        				//Bukkit.getLogger().info("Incoming damage is " + receivedDamage.toString());
        				receivedDamage = (receivedDamage * .375);
        				if(Avistlo.isInFinalForm())
        				{// dividing by 3.75 makes received damage 10% of waht it used to be. This further lowers it, getting it to average of prot 4 full amror.
        					receivedDamage = receivedDamage / 5 ;
        				}
        				e.setDamage(receivedDamage);
        				//Bukkit.getLogger().info("Outgoing Damage is " + receivedDamage.toString());
            		}
        			
        			//PassiveTitanSkill HealSkill = AvistloPassiveHeal;
        			PassiveTitanSkill Pskill = Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity()).getPassiveskills("heal");
        			if(!Pskill.getRunning())
        			{
        				Pskill.start();
        			}
            	}
            }
    	}
    	if(e.getDamager() instanceof Player)
    	{
            if(Titans.plugin.isPlayerATitan((Player)e.getDamager()))
            {
            	if(Titans.plugin.getTitanAssignedToPlayer((Player)e.getDamager()).getName().equalsIgnoreCase("Avistlo"))
        		{
            		if(Avistlo.isInFinalForm())
            		{
            			//Not sure if this works for bows. Need to test on server, I assume it will though
            			Double IncomingDamage = e.getDamage();
            			IncomingDamage = IncomingDamage * 1.5;
            			e.setDamage(IncomingDamage);
            		}
            		
        		}
            }
    	}
    }
    @EventHandler
    public void OnHunger(FoodLevelChangeEvent e)
    {
    	if(Titans.plugin.isPlayerATitan((Player)e.getEntity()))
        {
        	//Probably should check if titan equals avistlo, not just the name, but dunno how.
    		if(Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity()).getName().equalsIgnoreCase("Avistlo"))
    		{
    			/*Value after hunger change*/
    			//Bukkit.getLogger().info(String.valueOf(e.getFoodLevel()));
    			int AfterChange = e.getFoodLevel();
    			Player p = (Player)e.getEntity();
    			/* Value before Hunger Change*/
    			//Bukkit.getLogger().info(String.valueOf(p.getFoodLevel()));
    			int Current = p.getFoodLevel();
    			if(AfterChange < Current)
    			{
    				Double Chance = Math.random();
                    if (Chance < .8D)
                    {
                        e.setFoodLevel(Current);
                    }
                }
    		}
    	}
    }
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageEvent(final EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) {
        	return;
        }
    	Player p = (Player) e.getEntity();
        if(Titans.plugin.isPlayerATitan(p))
        {
        	Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity());
        	if(titan.getName().equalsIgnoreCase("Avistlo"))
        	{
    			PassiveTitanSkill Pskill = Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity()).getPassiveskills("heal");
    			if(!Pskill.getRunning())
    			{
    				Pskill.start();
    			}
        	}
        }
    }
}
