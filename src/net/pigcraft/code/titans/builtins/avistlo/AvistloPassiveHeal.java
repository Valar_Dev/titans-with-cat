//This file is discussed in a PM between me and TB, and not on the thread.
package net.pigcraft.code.titans.builtins.avistlo;

import org.bukkit.entity.Player;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Passive;
import net.pigcraft.code.titans.skills.PassiveTitanSkill;

public final class AvistloPassiveHeal extends PassiveTitanSkill
{
	Player player;
	Passive passive = new Passive(1, this);
	boolean isrunning = false;
	Titan titan;
    public AvistloPassiveHeal(final Titan titan)
    {
        super("heal",titan);
        this.titan = titan;
    }

	@Override
    public void use()
    {
		//Player player = this.titan.getController();
		double Chance = Math.random();
		if(Chance >= .5)
		{
			player.setHealth(player.getHealth() + 1);
		}
		if(Avistlo.isInFinalForm() && Chance <= .33)
			player.setHealth(player.getHealth() + 1);
			
    }

	@Override
	public boolean Continue() {
		//Player player = this.titan.getController();
		if(player.getHealth() > (player.getMaxHealth() / 2))
		{
			this.setRunning(false);
			return false;
		}
		return true;
	}

	@Override
	public void start() 
	{
		player = this.titan.getController();
		if(player.getHealth() < (player.getMaxHealth() / 2))
		{
			this.setRunning(true);
			passive.startSkill();
			RoarSkill.use(player);
		}
	}
}

