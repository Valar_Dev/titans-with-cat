package net.pigcraft.code.titans.builtins.avistlo;

import java.util.ArrayList;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public final class GiftOfFateSkill extends TitanSkill
{
    public GiftOfFateSkill(final Titan titan)
    {
        super("gift", titan, new Cooldown(20));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Gift of Fate"
        + "\n           Avistlo and all nearby are partially healed.");
        String[] Aliases = {"FateGift", "curse", "gFate", "fg", "GiftOfFate"};
        this.setAltNames(Aliases);
    }

    @Override
    public void use()
    {
    	if(this.getTitan().getController().getWorld().getTime() <= 12000)
    	{
    	int counter = 0;
        ArrayList<Player> nearbyPlayers = playersDistance(this.getTitan().getController().getLocation(), 9);
        for(Player player : nearbyPlayers)
        {
        	counter++;
        	if(player.getHealth() + 10 < player.getMaxHealth())
        	{
        		player.setHealth(player.getHealth() + 10);
        	}
        	else
        		player.setHealth(player.getMaxHealth());
        	player.setFoodLevel(player.getFoodLevel() + 10);
        	player.sendMessage(ChatColor.GRAY + "You have received a lucky gift of fate from" + ChatColor.DARK_GRAY + " Skylord Avistlo!");
        }
        this.getTitan().getController().sendMessage(ChatColor.GOLD + "You healed " + counter + " players Avistlo!");
        this.getTitan().getController().setFireTicks(0);
        //The Next line is optional, heals Avistlo for extra. I will ask TB about it.
        //this.getTitan().getController().setHealth(this.getTitan().getController().getHealth() + 10);
    	}
    	else if(this.getTitan().getController().getWorld().getTime() > 12000)
    	{
    		int counter = 0;
            ArrayList<Player> nearbyPlayers = playersDistance(this.getTitan().getController().getLocation(), 13);
            for(Player player : nearbyPlayers)
            {
            	if (player != this.getTitan().getController())
            	{
            		player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 0));
            		player.sendMessage(ChatColor.GRAY + "You have been cursed by fate - "
            		+ ChatColor.DARK_GRAY + "Skylord Avistlo");
            		counter++;
            	}
            	else if(player == this.getTitan().getController())
            	{
            		player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 120, 0));
            	}
            }
            this.getTitan().getController().sendMessage(ChatColor.DARK_PURPLE + "" +counter + "player(s) have been cursed by fate");
    	}
    }
    
    
    //Thank you nitrousspark !
    //https://forums.bukkit.org/threads/getting-all-players-within-a-radius-of-a-player.138554/
    public static ArrayList<Player> playersDistance(Location loc,
    		double radius) {
    		 
    		ArrayList<Player> players = new ArrayList<Player>();
    		 
    		double i1 = loc.getX();
    		double j1 = loc.getY();
    		double k1 = loc.getZ();
    		 
    		for (Player player : Bukkit.getOnlinePlayers()) {
    		 
    		if (player.getWorld().equals(loc.getWorld())) {
    		 
    		double i2 = player.getLocation().getX();
    		double j2 = player.getLocation().getY();
    		double k2 = player.getLocation().getZ();
    		 
    		double ad = Math.sqrt((i2 - i1)*(i2 - i1) + (j2 - j1)*(j2 - j1) + (k2 - k1)*(k2 - k1));
    		 
    		if (ad < radius) {
    		players.add(player);
    		}
    		 
    		}
    		 
    		}
    		 
    		return players;
    		 
    		}
}
