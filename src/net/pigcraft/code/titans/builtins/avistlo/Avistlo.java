package net.pigcraft.code.titans.builtins.avistlo;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public final class Avistlo extends Titan
{

    private double prevHealth;
    ArrayList<EntityType> MobProtections;
    ArrayList<DamageCause> WorldProtections;
    private String prevDisplayName;
    private float prevWalkSpeed;
    World world;
    private static boolean finalForm = false;
    
    public Avistlo()
    {
        super
        (
                Titans.plugin,
                "Avistlo", "Fate and Destiny begin to change as Avistlo appears",
                "Everything calms as Avistlo vanishes from the world.",
                ChatColor.DARK_GRAY + "Avistlo " + ChatColor.GRAY + "<message>"
        );

        this.setDescription(" Avistlo is the Titan of Light and Fate." +
        " Brother to the Titan of Reality and Life," +
        " he came into existence near the beginning of time." + 
        " Avistlo travels the world and skies, changing"
        +"the Fate of those he finds based on the Light around them."+
        " He is known to give gifts or curses when sought out so be wary should you ever meet.");
        
        MobProtections = new ArrayList<EntityType>();
        MobProtections.add(EntityType.ZOMBIE);
        MobProtections.add(EntityType.CREEPER);
        MobProtections.add(EntityType.SKELETON);
        MobProtections.add(EntityType.WITHER_SKULL);
        MobProtections.add(EntityType.WITHER);
        MobProtections.add(EntityType.ENDER_DRAGON);
        MobProtections.add(EntityType.SLIME);
        MobProtections.add(EntityType.SILVERFISH);
        MobProtections.add(EntityType.ENDERMAN);
        MobProtections.add(EntityType.CAVE_SPIDER);
        MobProtections.add(EntityType.ENDER_CRYSTAL);
        MobProtections.add(EntityType.SPIDER);
        this.SetMobProtectionList(MobProtections);
        
        WorldProtections = new ArrayList<DamageCause>();
        WorldProtections.add(DamageCause.LIGHTNING);
        WorldProtections.add(DamageCause.FIRE);
        WorldProtections.add(DamageCause.FIRE_TICK);
        WorldProtections.add(DamageCause.FALL);
        WorldProtections.add(DamageCause.FALLING_BLOCK);
        WorldProtections.add(DamageCause.VOID);
        WorldProtections.add(DamageCause.LAVA);
        this.SetWorldProtectionList(WorldProtections);
        
        setFinalDrops(this);
    }

    @Override
    public void onEntrance()
    {
    	world = this.getController().getWorld();
    	this.prevWalkSpeed = this.getController().getWalkSpeed();

        this.prevDisplayName = this.getController().getDisplayName();
        this.getController().setFireTicks(0);
        this.prevHealth = this.getController().getMaxHealth();
        this.getController().setMaxHealth(280D);
        this.getController().setHealth(280D);
        this.getController().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2000000, 1));
        this.getController().setWalkSpeed(.45f);
        this.getController().setFoodLevel(this.getController().getFoodLevel() + 8);
        equipWeapons(this.getController());
        if(world.getTime() >= 12000){
            world.strikeLightning(this.getController().getLocation().subtract(15, 0, 0));
            world.setStorm(true);
            for (Player player : this.getController().getWorld().getPlayers())
            {
                this.getController().getWorld().playSound(player.getLocation(), Sound.AMBIENCE_THUNDER, 1, 0);
            }
        }
        else{
        	world.setStorm(false);
            for (Player player : this.getController().getWorld().getPlayers())
            {
                this.getController().getWorld().playSound(player.getLocation(), Sound.AMBIENCE_CAVE, 1, 0);
            }
        }
    }

    @Override
    public void onExit()
    {
        for (Player player : this.getController().getWorld().getPlayers())
        {
            this.getController().getWorld().playSound(player.getLocation(), Sound.PORTAL_TRAVEL, 1, 0);
        }
        
        this.getController().setHealth(this.prevHealth);
        this.getController().setMaxHealth(this.prevHealth);
        this.getController().removePotionEffect(PotionEffectType.JUMP);
        this.getController().setDisplayName(this.prevDisplayName);
        this.getController().setWalkSpeed(this.prevWalkSpeed);
        world.setStorm(false);
        removeWeapons(this.getController());
    }

    @Override
    public void onLoad()
    {
    	this.addSkill(new GiftOfFateSkill(this));
    	this.addSkill(new ForceOfLight(this));
    	this.addSkill(new ContradictFateSkill(this));
    	this.addSkill(new WindsOfChangeSkill(this));
    	this.addSkill(new LighterThanAirSkill(this));
    	//if(Titans.plugin.getServer().getPluginManager().getPlugin("MagicSpells") != null)
    	{
    		this.addSkill(new ReaperShiftSkill(this));
    	}
    	/*else
    	{
    		this.addSkill(new ReaperShiftNoDisguiseCraft(this));
    	}*/
    	Titans.plugin.getServer().getPluginManager().registerEvents(new WeaponListener(), Titans.plugin);
    	Titans.plugin.getServer().getPluginManager().registerEvents(new AvistloIncomingDamageListener(), Titans.plugin);
    	//Titans.plugin.getServer().getPluginManager().registerEvents(new ReaperShiftListener(), Titans.plugin);
    	this.addPassiveSkill(new AvistloPassiveHeal(this));
    }

    @Override
    public void onUnload()
    {
    	this.clearSkills();
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /** 
     *  The player is equipped with the weapons Soul Blade and Piercing Light, and when they 
     *  are Avistlo, have special effects.  **/
    public static void equipWeapons(Player p)
    {
    	ItemStack SoulBlade = new ItemStack(Material.IRON_SWORD);
    	SoulBlade.addEnchantment(Enchantment.DAMAGE_ALL, 5);
    	SoulBlade.addEnchantment(Enchantment.FIRE_ASPECT, 2);
    	SoulBlade.setDurability(Short.MIN_VALUE);
    	ItemMeta meta = SoulBlade.getItemMeta();
    	meta.setDisplayName(ChatColor.DARK_GRAY + "Soul Blade");
    	ArrayList<String> lore = new ArrayList<String>();
    	lore.add(ChatColor.AQUA + "Iron Soul Stealer: Soul Reaver");
    	lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + 
    			"20 percent chance on hit to do a critical");
    	lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + "and to steal their soul for 3 seconds");
    	lore.add("");
    	lore.add("When A player is missing their soul, they have nausea");
    	meta.setLore(lore);
    	SoulBlade.setItemMeta(meta);
    	p.getInventory().addItem(SoulBlade);
    	
    	ItemStack PiercingLight = new ItemStack(Material.BOW);
    	PiercingLight.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
    	PiercingLight.addEnchantment(Enchantment.ARROW_INFINITE, 1);
    	PiercingLight.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 2);
    	PiercingLight.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
    	meta = PiercingLight.getItemMeta();
    	meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.ITALIC + "Piercing Light");
    	lore.clear();
    	lore.add(ChatColor.YELLOW + "Brilliant Light");
    	lore.add(ChatColor.GRAY + "A 25 percent chance to daze the");
    	lore.add(ChatColor.GRAY + "target with a Brilliant Light!");
    	lore.add(ChatColor.YELLOW + "Shimmering Light");
    	lore.add(ChatColor.GRAY + "A 25% chance to unleash the light energy");
    	lore.add(ChatColor.GRAY + "in your arrows to a shimmering loud boom");
    	meta.setLore(lore);
    	PiercingLight.setItemMeta(meta);
    	p.getInventory().addItem(PiercingLight);
    	p.getInventory().addItem(new ItemStack(Material.ARROW));
    }
    
    public static void removeWeapons(Player p)
    {
    	//This method took way longer to right then you'd think xD
    	ItemStack[] contents = p.getInventory().getContents();
    	ItemStack[] airlessContents;
    	boolean takenArrow = false;
    	ArrayList<ItemStack> isList = new ArrayList<ItemStack>();
    	for(ItemStack is : contents)
    	{
            if (is != null && is.getType() != Material.AIR)
            {
    			isList.add(is);
    		}
        }
    	airlessContents = isList.toArray(new ItemStack[isList.size()]);
    	for(ItemStack inv : airlessContents)
    	{
    		if(inv.hasItemMeta())
    		{
    			if(inv.getType() == Material.IRON_SWORD )
    			{
    				if(inv.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GRAY + 
    						"Soul Blade"))
    					{
    						p.getInventory().remove(inv);
    					}
    			}
    			else if(inv.getType() == Material.BOW)
    			{
    			if(inv.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" +
    					ChatColor.ITALIC + "Piercing Light"))
					{
    				p.getInventory().remove(inv);
					}
    			}
    		}
    		else if(inv.getType() == Material.ARROW && !takenArrow){
    			takenArrow = true;
    			if(inv.getAmount() == 1)
    			{
    				p.getInventory().remove(inv);
    			}
    			else
    			{
    				inv.setAmount(inv.getAmount() - 1);
    			}
    		}
    	}
    }

    public static boolean isInFinalForm()
    {
        return finalForm;
    }

    public static void setInFinalForm(final boolean flag)
    {
        finalForm = flag;
    }

    public static void setFinalDrops(Titan tit)
    {
    	List<ItemStack> Drops = new ArrayList<ItemStack>();
    	ItemStack MirrorShield = new ItemStack(Material.GOLD_SWORD);
    	MirrorShield.addUnsafeEnchantment(Enchantment.DURABILITY, 33);
    	ItemMeta meta = MirrorShield.getItemMeta();
    	meta.getLore().add("This shield was forged by the \n dwarves from a time Long Forgotten,");
    	meta.getLore().add("the Mirror Shield reflects damage \n right back to your opponent!");
    	meta.getLore().add("You see a perfect reflection of yourself \n when gazing at this mystical shield.");
    	meta.getLore().add("");
    	meta.getLore().add("Reflect:40");
    	meta.getLore().add("SetDamage:5");
    	meta.setDisplayName("Mirror Shield");
    	MirrorShield.setItemMeta(meta);
    	Drops.add(MirrorShield);
    	
    	ItemStack LuckyRod = new ItemStack(Material.FISHING_ROD);
    	LuckyRod.addUnsafeEnchantment(Enchantment.LURE, 6);
    	meta = LuckyRod.getItemMeta();
    	meta.getLore().add("This lucky fishing rod is bound to help \n you get some great loot!");
    	meta.setDisplayName("Lucky Rod");
    	LuckyRod.setDurability((short) (LuckyRod.getDurability() + 30));
    	LuckyRod.setItemMeta(meta);
    	Drops.add(LuckyRod);
    	
    	tit.SetFinalDrops(Drops);
    }
}
