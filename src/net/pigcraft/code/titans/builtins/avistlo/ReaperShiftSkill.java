package net.pigcraft.code.titans.builtins.avistlo;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


import java.util.ArrayList;

public final class ReaperShiftSkill extends DurationTitanSkill
{
	Player player;
	boolean changedToStorm;
    Duration duration = new Duration(70, this);
    World world;
    int task;
    public ReaperShiftSkill(final Titan titan)
    {
        super("Shift", titan, new Cooldown(180));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Reaper Shift"
        + "\n          The weather in the world changes to help or hinder travelers.");
        String[] Aliases = {"ReaperShift", "Reaper", "shif", "final", "form"};
        this.setAltNames(Aliases);
    }
    
    
    @Override
    public void use()
    {
    	player = this.getTitan().getController();
    	player.setWalkSpeed(.9f);
    	world = player.getWorld();
    	player.setAllowFlight(true);
    	player.setFlying(true);
    	player.setFlySpeed(.155f);
    	player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 70*20, 1));
        ///this.getTitan().setInFinalForm(true);
    	Avistlo.setInFinalForm(true);
        Avistlo.removeWeapons(player);
        AddShiftWeapons(player);
        duration.startDuration();
        stopTime();
    }
    public void stopTime()
    {
    	world.setTime(20000);
	      task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Titans.plugin, new Runnable()
	      {
	        @Override
	        public void run()
	        {
	        	world.setTime(world.getTime() - 100);
	            if(Avistlo.isInFinalForm()){
	            	Bukkit.getScheduler().cancelTask(task);
	            }
	        }
	      }, 100, 100);
    }

	@Override
	public void DurationDone() {
        player.setWalkSpeed(.45f);
		player.setFlySpeed(.1f);
		player.setFlying(false);
		player.setAllowFlight(false);
        removeShiftWeapons(player);
        Avistlo.equipWeapons(player);
    	Avistlo.setInFinalForm(false);
	}

	
	public static void AddShiftWeapons(Player p) {
		ItemStack BladeOf8thGate = new ItemStack(Material.DIAMOND_SWORD);
		BladeOf8thGate.addEnchantment(Enchantment.DAMAGE_ALL, 5);
		BladeOf8thGate.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 7);
		BladeOf8thGate.addEnchantment(Enchantment.FIRE_ASPECT, 2);
		BladeOf8thGate.setDurability(Short.MIN_VALUE);
		ItemMeta meta = BladeOf8thGate.getItemMeta();
		meta.setDisplayName(ChatColor.DARK_RED + "Blade of the" + ChatColor.DARK_GRAY 
				+ "8th Gate");
    	ArrayList<String> lore = new ArrayList<String>();
    	lore.add(ChatColor.DARK_GRAY + "Shroud of the 8th Gate");
    	lore.add(ChatColor.GRAY + "25% chance to blind the target for 3sec");
    	lore.add(ChatColor.DARK_GRAY + "Doors of the 8th Gate");
    	lore.add("Left Door Opens: 17% chance to blind the target and do critical dmg");
    	lore.add("Right Door Opens: 17% chance to wither target and do critical dmg");
    	meta.setLore(lore);
    	BladeOf8thGate.setItemMeta(meta);
    	p.getInventory().addItem(BladeOf8thGate);
    	
    	ItemStack DeathShard = new ItemStack(Material.BOW);
    	DeathShard.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
    	DeathShard.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 6);
    	DeathShard.addEnchantment(Enchantment.ARROW_INFINITE, 1);
    	DeathShard.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
    	DeathShard.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 2);
    	meta = DeathShard.getItemMeta();
    	lore.clear();
    	meta.setDisplayName(ChatColor.BLACK + "" + ChatColor.ITALIC + "Death Shard");
    	lore.add(ChatColor.AQUA + "TO DO");
    	DeathShard.setItemMeta(meta);
    	p.getInventory().addItem(DeathShard);
    	p.getInventory().addItem(new ItemStack(Material.ARROW));
	}
	
    public static void removeShiftWeapons(Player p)
    {
    	//This method took way longer to right then you'd think xD
    	ItemStack[] contents = p.getInventory().getContents();
    	ItemStack[] airlessContents;
		boolean takenArrow = false;
    	ArrayList<ItemStack> isList = new ArrayList<ItemStack>();
    	for(ItemStack is : contents)
    	{
            if (is != null && is.getType() != Material.AIR)
            {
    			isList.add(is);
    		}
        }
    	airlessContents = isList.toArray(new ItemStack[isList.size()]);
    	for(ItemStack inv : airlessContents)
    	{
    		if(inv.hasItemMeta()){
    			if(inv.getType() == Material.DIAMOND_SWORD)
    			{
    				if(inv.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + 
    						"Blade of the" + ChatColor.DARK_GRAY + "8th Gate"))
    					{
    						p.getInventory().remove(inv);
    					}
    			}
    			else if(inv.getType() == Material.BOW)
    			{
    				if(inv.getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLACK +
    						"" + ChatColor.ITALIC + "Death Shard"))
    				{
    					p.getInventory().remove(inv);
    				}
    			}
    			else if(inv.getType() == Material.ARROW && !takenArrow){
    				takenArrow = true;
    				if(inv.getAmount() == 1)
    				{
    					p.getInventory().remove(inv);
    				}
    				else
    				{
    					inv.setAmount(inv.getAmount() - 1);
    				}
    			}
    		}
    	}
    }
}
