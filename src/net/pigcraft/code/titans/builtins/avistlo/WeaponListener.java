package net.pigcraft.code.titans.builtins.avistlo;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.PassiveTitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("unused")
public final class WeaponListener implements Listener
{
    @EventHandler
    public void onDamager(EntityDamageByEntityEvent e){
    	if(e.getDamager() instanceof Player)
    	{
            if(Titans.plugin.isPlayerATitan((Player)e.getDamager()))
            {
            	if(Titans.plugin.getTitanAssignedToPlayer((Player)e.getDamager()).getName().equalsIgnoreCase("Avistlo"))
        		{
            		Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)e.getDamager());
            		double damage = e.getDamage();
            		if(titan.getController().getItemInHand().hasItemMeta()){
            		if(titan.getController().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GRAY + "Soul Blade"))
            		{
            			double Chance = Math.random();
            			if(Chance >= .7)
            			{

            				damage = damage +(damage/2 + 2);
            				if(e.getEntity() instanceof Player)
            				{
            					Player p = (Player)e.getEntity();
            					p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 60, 1));
            				}
            			}
        				if(e.getEntity() instanceof Player)
            				damage += CommonWepAttrib((Player)e.getEntity());
        				e.setDamage(damage);
            		}
            		else if(titan.getController().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Blade of the" +
            		ChatColor.DARK_GRAY + "8th Gate"))
            		{
            			double Chance = Math.random();
        				
        				damage += 3D;
        				if(Chance >= .75)
        				{
        					if(e.getEntity() instanceof LivingEntity)
        					{
        						((LivingEntity) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,
        								60, 1));
        					}
        				}
        				else if(Chance <= .50 && Chance >= .15)
        				{
        					if(e.getEntity() instanceof LivingEntity)
        					{
        						//Do Wither 
        						if(Chance > .325)
        						{
        							((LivingEntity)e.getEntity()).addPotionEffect(new PotionEffect(
        									PotionEffectType.WITHER, 60, 1));
        						}
        						//Do Blind
        						else
        							((LivingEntity)e.getEntity()).addPotionEffect(new PotionEffect(
        									PotionEffectType.BLINDNESS, 60, 1));
                				damage = damage +(damage/2 + 2);
        					}
        				}
        				if(e.getEntity() instanceof Player)
            				damage += CommonWepAttrib((Player)e.getEntity());
        				e.setDamage(damage);
            		}
        		}
            		if(titan.getController().getHealth() * 2 < titan.getController().getMaxHealth())
            			e.setDamage(damage + 2);
            }
            }
    	}
    	if(e.getDamager() instanceof Arrow)
    	{
    		Arrow arrow = (Arrow)e.getDamager();
    		if(arrow.getShooter() instanceof Player)
    		{
    			 if(Titans.plugin.isPlayerATitan((Player)arrow.getShooter()))
    	            {
    	            	if(Titans.plugin.getTitanAssignedToPlayer((Player)arrow.getShooter()).getName().equalsIgnoreCase("Avistlo"))
    	        		{
    	            		Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)arrow.getShooter());
    	            		double Damage = e.getDamage();
    	            		if(titan.getController().getItemInHand().hasItemMeta()){
    	            		if(titan.getController().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.ITALIC + "Piercing Light"))
    	            		{
    	            			double Chance = Math.random();
    	            			if(Chance >= .75)
    	            			{
    	            				if(e.getEntity() instanceof Player)
    	            				{
    	            					Player p = (Player)e.getEntity();
    	            					p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 60, 2));
    	            				}
    	            				if(Chance >= .82)
    	            					e.getEntity().getWorld().strikeLightningEffect(e.getEntity().getLocation());
    	            			}
    	            			else if(Chance <= .14)
    	            				e.getEntity().getWorld().strikeLightningEffect(e.getEntity().getLocation());
    	        				if(e.getEntity() instanceof Player)
    	            				Damage += CommonWepAttrib((Player)e.getEntity());
    	        				e.setDamage(Damage);
    	            		}
    	            		else if(titan.getController().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.BLACK 
    	            				+ "" + ChatColor.ITALIC + "Death Shard"))
    	            		{
    	            			if(e.getEntity() instanceof LivingEntity)
    	            			{
    	            				double Chance = Math.random();
    	            				LivingEntity Target = (LivingEntity)e.getEntity();
    	            				if(Chance >= .66)
    	            				{
    	            					Target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 1));
    	            				}
    	            				else if(Chance <= .33)
    	            					Target.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 60, 1));
    	            				else
    	            				{
    	            					Target.getWorld().strikeLightningEffect(Target.getLocation());
    	            					Damage += 2D;
    	            				}
        	        				if(Target instanceof Player)
        	            				Damage += CommonWepAttrib((Player)Target);
        	        				Damage += 2D;
        	        				e.setDamage(Damage);
    	            			}
    	            		}
    	        		}
    	            		if(titan.getController().getHealth() * 2 < titan.getController().getMaxHealth())
    	            			e.setDamage(Damage + 2);
    	            		}
    	            }
    		}
    	}
    }
    public double CommonWepAttrib(Player p)
    {
    	double DamageAdder = 2;
    	ItemStack[] armor = p.getInventory().getArmorContents();
    	for(ItemStack piece : armor)
    	{
    		String check = piece.getType().toString();
    		Bukkit.getLogger().info(check);
    		if(check.contains("DIAMOND")){
    			DamageAdder += 1.75D;
    		}
    		else if(check.contains("GOLD"))
    		{
    			DamageAdder += .5D;
    		}
    		else if(check.contains("IRON"))
    		{
    			DamageAdder += 1D;
    		}
    		
    		if(piece.containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
    		{
    			DamageAdder += (piece.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL) * 1D);
    		}
    		Bukkit.getLogger().info(String.valueOf(DamageAdder));
    	}
    	return DamageAdder;
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void OnEntityDeath(EntityDeathEvent e)
    {
    	Player killer = e.getEntity().getKiller();
    	if(killer != null)
    	{
    		if(Titans.plugin.isPlayerATitan(killer))
    		{
    			if(Titans.plugin.getTitanAssignedToPlayer(killer).getName().equalsIgnoreCase("Avistlo"))
    			{
    				Titan tit = Titans.plugin.getTitanAssignedToPlayer(killer);
    				if(tit.getController().getItemInHand().hasItemMeta()){
    				String it = tit.getController().getItemInHand().getItemMeta().getDisplayName();
    				if(it.endsWith("Soul Blade") || it.endsWith("Piercing Light") ||
    						it.endsWith("8th Gate") || it.endsWith("Death Shard")){
    					tit.getController().getWorld().strikeLightningEffect(e.getEntity().getLocation());
    				}}
    			}
    		}
    	}
    }
}
