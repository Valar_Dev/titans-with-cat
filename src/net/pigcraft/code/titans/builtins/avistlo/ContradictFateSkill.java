package net.pigcraft.code.titans.builtins.avistlo;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.TitanSkill;

public final class ContradictFateSkill extends TitanSkill
{
    public ContradictFateSkill(final Titan titan)
    {
        super("ConFate", titan, new Cooldown(15));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Contradict Fate"
        + "\n           Avistlo undoes the Fate of past beings and resurects" + 
        		"\n           monsters.");
        String[] Aliases = {"con", "cfate", "fate", "ContradictFate"};
        this.setAltNames(Aliases);
    }

    @SuppressWarnings("deprecation")
	@Override
    public void use()
    {
    	Player controller = this.getTitan().getController();
    	Location loc = this.getTitan().getController().getLocation();
    	
    	controller.getWorld().spawnEntity(controller.getTargetBlock(null, 200).getLocation(), EntityType.ZOMBIE);
    	controller.getWorld().spawnEntity(controller.getTargetBlock(null, 200).getLocation(), EntityType.SKELETON);
    	
    	Location spawn1 = new Location(loc.getWorld(), loc.getX() - 1,loc.getY(), loc.getZ());
    	Location spawn2 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() + 1);
    	Location spawn3 = new Location(loc.getWorld(), loc.getX(),loc.getY(), loc.getZ() - 1);
    	Location spawn4 = new Location(loc.getWorld(), loc.getX() + 1,loc.getY(), loc.getZ());
		controller.getWorld().spawnEntity(spawn1, EntityType.ZOMBIE);
		controller.getWorld().spawnEntity(spawn2, EntityType.ZOMBIE);

		Skeleton skeleton = (Skeleton) (controller.getWorld().spawnEntity(spawn3, EntityType.SKELETON));
		equipSkeleton(skeleton);
		Skeleton skeleton2 = (Skeleton) (controller.getWorld().spawnEntity(spawn4, EntityType.SKELETON));
		equipWitherSkeleton(skeleton2);
		
		for(Location locz : playersDistance(controller.getLocation(), 20))
			{
				Double Chance = Math.random();
				if(Chance > .26 && Chance < .5)
				{
					Location PSpawn1 = new Location(locz.getWorld(), locz.getX() - 6, loc.getY(), loc.getZ());
					Skeleton Pskeleton1 = (Skeleton) (controller.getWorld().spawnEntity(PSpawn1, EntityType.SKELETON));
					equipWitherSkeleton(Pskeleton1);
					Location PSpawn2 = new Location(locz.getWorld(), locz.getX() + 3, loc.getY(), loc.getZ() - 2);
					Zombie zombie = (Zombie) controller.getWorld().spawnEntity(PSpawn2, EntityType.ZOMBIE);
					armorSkeleton(zombie);
					Location PSpawn3 = new Location(locz.getWorld(), locz.getX() - 3, loc.getY(), loc.getZ() + 4);
					Skeleton Pskeleton2 = (Skeleton) (controller.getWorld().spawnEntity(PSpawn3, EntityType.SKELETON));
					equipSkeleton(Pskeleton2);
				}
				else if(Chance > .62)
				{
					Location PSpawn1 = new Location(locz.getWorld(), locz.getX() + 8, loc.getY(), loc.getZ() - 2);
					Skeleton Pskeleton1 = (Skeleton) (controller.getWorld().spawnEntity(PSpawn1, EntityType.SKELETON));
					equipSkeleton(Pskeleton1);
					
					Location PSpawn2 = new Location(locz.getWorld(), locz.getX() + 2, loc.getY(), loc.getZ() - 4);
					Zombie zombie2 = (Zombie) controller.getWorld().spawnEntity(PSpawn2, EntityType.ZOMBIE);
					armorSkeleton(zombie2);
					
					Location PSpawn3 = new Location(locz.getWorld(), locz.getX() + 5, loc.getY(), loc.getZ() - 6);
					Skeleton Pskeleton2 = (Skeleton) (controller.getWorld().spawnEntity(PSpawn3, EntityType.SKELETON));
					equipWitherSkeleton(Pskeleton2);
					
					Location PSpawn4 = new Location(locz.getWorld(), locz.getX() - 7, loc.getY() + 1, loc.getZ());
					Skeleton Pskeleton3 = (Skeleton) (controller.getWorld().spawnEntity(PSpawn4, EntityType.SKELETON));
					equipWitherSkeleton(Pskeleton3);
					Zombie zombie = (Zombie) controller.getWorld().spawnEntity(PSpawn4, EntityType.ZOMBIE);
					armorSkeleton(zombie);
				}
				else if(Chance < .25)
				{
			    	Location Pspawn1 = new Location(locz.getWorld(), locz.getX() - 2,loc.getY(), loc.getZ() - 1);
			    	Location Pspawn2 = new Location(locz.getWorld(), locz.getX() + 1,loc.getY(), loc.getZ() + 2);
			    	Zombie zombie = (Zombie) controller.getWorld().spawnEntity(Pspawn1, EntityType.ZOMBIE);
			    	Zombie zombie2 = (Zombie) controller.getWorld().spawnEntity(Pspawn2, EntityType.ZOMBIE);
			    	armorSkeleton(zombie);
			    	armorSkeleton(zombie2);
				}
			}
		
    }
    public void equipSkeleton(Skeleton skeleton) {
    	ItemStack bow = new ItemStack(Material.BOW);
    	bow.addEnchantment(Enchantment.ARROW_FIRE, 1);
    	bow.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
    	bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
    	skeleton.getEquipment().setItemInHand(bow);
    	skeleton.setSkeletonType(SkeletonType.NORMAL);
    	armorSkeleton(skeleton);
    	}
    public void equipWitherSkeleton(Skeleton skeleton) {
    	ItemStack sword = new ItemStack(Material.STONE_SWORD);
    	sword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
    	sword.addEnchantment(Enchantment.DAMAGE_ALL, 3);
    	sword.addEnchantment(Enchantment.KNOCKBACK, 1);
    	skeleton.getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD));
    	skeleton.setSkeletonType(SkeletonType.WITHER);
    	armorSkeleton(skeleton);
    	}
    public void armorSkeleton(LivingEntity skeleton)
    {
    	Random random = new Random();
    	int chance = random.nextInt(3);
    	if(chance == 0)
    	{
    		skeleton.getEquipment().setBoots(Enchanter(new ItemStack(Material.LEATHER_BOOTS), 1));
    		skeleton.getEquipment().setChestplate(Enchanter(new ItemStack(Material.LEATHER_CHESTPLATE), 1.1));
    		skeleton.getEquipment().setLeggings(Enchanter(new ItemStack(Material.LEATHER_LEGGINGS), 1));
    	}
    	else if(chance == 1)
    	{
    		skeleton.getEquipment().setChestplate(Enchanter(new ItemStack(Material.IRON_CHESTPLATE), 1));
    		skeleton.getEquipment().setLeggings(Enchanter(new ItemStack(Material.IRON_LEGGINGS), .9));
    	}
    	else if(chance == 2)
    	{
    		chance = random.nextInt(2);
    		if(chance == 1)
    			skeleton.getEquipment().setChestplate(Enchanter(new ItemStack(Material.DIAMOND_CHESTPLATE), .8));
    		if(chance == 0)
    			skeleton.getEquipment().setChestplate(Enchanter(new ItemStack(Material.GOLD_CHESTPLATE), 1.2));
    	}
    }
    public ItemStack Enchanter(ItemStack item, double chance)
    {
    	double rand = Math.random();
    	if(rand < .41*chance)
    	{
    		if(rand < .16*chance)
    		{
    			item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
    		}
    		else
    		{
    			item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);    			
    		}
    	}
    	return item;
    }
    public static ArrayList<Location> playersDistance(Location loc,
    		double radius) {
    		 
    		ArrayList<Player> players = new ArrayList<Player>();
    		ArrayList<Location> locs = new ArrayList<Location>();
    		double i1 = loc.getX();
    		double j1 = loc.getY();
    		double k1 = loc.getZ();
    		 
    		for (Player player : Bukkit.getOnlinePlayers()) {
    		 
    		if (player.getWorld().equals(loc.getWorld())) {
    		 
    		double i2 = player.getLocation().getX();
    		double j2 = player.getLocation().getY();
    		double k2 = player.getLocation().getZ();
    		 
    		double ad = Math.sqrt((i2 - i1)*(i2 - i1) + (j2 - j1)*(j2 - j1) + (k2 - k1)*(k2 - k1));
    		 
    		if (ad < radius) {
    		players.add(player);
    		locs.add(player.getLocation());
    		}
    		 
    		}
    		 
    		}
    		 
    		return locs;
    		 
    	}
	}

