package net.pigcraft.code.titans.builtins.avistlo;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

public final class WindsOfChangeSkill extends DurationTitanSkill
{
	boolean changedToStorm;
    Duration duration = new Duration(600, this);
    World world;
    public WindsOfChangeSkill(final Titan titan)
    {
        super("Winds", titan, new Cooldown(100));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Winds of Change"
        + "\n          The weather in the world changes to help or hinder travelers.");
        String[] Aliases = {"weather", "wind", "storm", "clear", "win"};
        this.setAltNames(Aliases);
    }
    
    
    @Override
    public void use()
    {
    	world = this.getTitan().getController().getWorld();
    	if(world.hasStorm())
    	{
    		world.setStorm(false);
    		world.setWeatherDuration(600 * 20);
    		changedToStorm = false;
    	}
    	else
    	{
    		world.setStorm(true);
    		world.setWeatherDuration(600*20);
    		world.setThundering(true);
    		world.setThunderDuration(600* 20);
    		changedToStorm = true;
    	}
    	Bukkit.getServer().dispatchCommand(this.getTitan().getController(), "cast tornado");
        duration.startDuration();
    }

	@Override
	public void DurationDone() {
		//this.getTitan().getController().sendMessage("This was claled");
		if (changedToStorm)
		{
			world.setStorm(false);
			world.setThundering(false);
		}
		else if(!changedToStorm)
		{
			world.setStorm(true);
			world.setThundering(true);
		}
		Bukkit.getLogger().info("Avistlo weather change is complete.");
	}
}
