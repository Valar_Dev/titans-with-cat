package net.pigcraft.code.titans.builtins.avistlo;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public final class RoarSkill
{

	public static void use(Player p)
    {
		p.sendMessage(ChatColor.DARK_RED +"Roar Used! Your health is at 50%, watch out!");
		Vector PV = p.getLocation().toVector();
		//exe. 330, 20, -59
		for(Entity pl : p.getNearbyEntities(10, 8, 10))
		{
			Vector newVector;
			if(pl.getLocation().getX() > p.getLocation().getX())
				newVector = pl.getLocation().toVector().add((PV.subtract(pl.getLocation().toVector())));
			else
				newVector = pl.getLocation().toVector().add((PV.subtract(pl.getLocation().toVector())).multiply(-1));
			
			while(newVector.getY() < 1)
				newVector.setY(newVector.getY() + 1.2);
			
			double magnitude = Math.sqrt(newVector.getX() * newVector.getX() + newVector.getZ() * newVector.getZ());
			
			magnitude = 7 / magnitude;
			newVector.setX(newVector.getX() * magnitude);
			newVector.setZ(newVector.getZ() * magnitude);
			pl.setVelocity(newVector);
			if(pl instanceof Player)
				((Player) pl).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 3, 600));
			
		}
    }
}
   
	
