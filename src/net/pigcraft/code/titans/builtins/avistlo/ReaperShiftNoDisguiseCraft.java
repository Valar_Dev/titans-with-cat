package net.pigcraft.code.titans.builtins.avistlo;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.skills.Cooldown;
import net.pigcraft.code.titans.skills.Duration;
import net.pigcraft.code.titans.skills.DurationTitanSkill;

public final class ReaperShiftNoDisguiseCraft extends DurationTitanSkill
{
	Player player;
	boolean changedToStorm;
    World world;
    Duration duration = new Duration(60, this);
    public ReaperShiftNoDisguiseCraft(final Titan titan)
    {
        super("Shift", titan,  new Cooldown(180));

        this.setDescription(ChatColor.GRAY + "Name: " + ChatColor.AQUA + "Winds of Change"
        + "\n          The weather in the world changes to help or hinder travelers.");
        String[] Aliases = {"ReaperShift", "Reaper", "shif", "final", "form"};
        this.setAltNames(Aliases);
    }
    
    
    @Override
    public void use()
    {
    	player = this.getTitan().getController();
    	player.setWalkSpeed(.9f);
    	player.setAllowFlight(true);
    	player.setFlying(true);
    	player.setFlySpeed(.155f);
    	player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1200, 1));
		Avistlo.setInFinalForm(true);
    	Avistlo.removeWeapons(player);
    	ReaperShiftSkill.AddShiftWeapons(player);
    	this.getTitan().getController().sendMessage("Disguise Craft is not installed so you do not transform.");
    	duration.startDuration();
    }


	@Override
	public void DurationDone() {
        player.setWalkSpeed(.45f);		
		player.setFlySpeed(.1f);
		player.setFlying(false);
		player.setAllowFlight(false);
        ReaperShiftSkill.removeShiftWeapons(player);
        Avistlo.equipWeapons(player);
        Avistlo.setInFinalForm(false);
	}
}
