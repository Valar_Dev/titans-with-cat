package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titans;

import org.bukkit.Bukkit;

public final class Duration
{
	
	private long durationLength;
	private DurationTitanSkill skill;
	
    public Duration(final long duration, final DurationTitanSkill skill)
	{
		this.durationLength = duration * 20L;
		this.skill = skill;
	}
	
	public void startDuration()
	{
		//Bukkit.getLogger().severe("asdffas");
		{
			Bukkit.getServer().getScheduler().runTaskLater(Titans.plugin, new Runnable(){

				@Override
				public void run() {
					skill.DurationDone();
				}
			}, this.durationLength);
		}
	}
}
