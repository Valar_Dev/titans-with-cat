package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;
import org.bukkit.event.Listener;

public abstract class TitanSkill implements Listener
{


    private final String name;
    private final Titan titan;
    private Cooldown cooldown;

    private String description;
    private String[] AltNames;

    /**Instantiates a titan skill with no cooldown.*/
    public TitanSkill(final String name, final Titan titan)
    {
        this.name = name;
        this.titan = titan;

        this.description = "";
    }

    /**Instantiates a TitanSkill with a cooldown.*/
    public TitanSkill(final String name, final Titan titan, final Cooldown cooldown)
    {
        this.name = name;
        this.titan = titan;
        this.cooldown = cooldown;

        this.description = "";
    }

    /**Returns the name of a Titan.*/
    public final String getName()
    {
        return name;
    }
    
    /**Returns all of the Skill's alternate names*/
    public final String[] getAltNames()
    {
    	return AltNames;
    }

    /**Returns the titan who called upon the skill */
    public final Titan getTitan()
    {
        return titan;
    }

    /**Returns the skill's description, or lore.*/
    public final String getDescription()
    {
        return description;
    }

    /**Returns the skill's instance of Cooldown.*/
    public final Cooldown getCooldown()
    {
        return cooldown;
    }

    /**Sets the skill's description or lore
     * 
     * @param String description, or lore*/
    public final void setDescription(final String description)
    {
        this.description = description;
    }

    /**Sets the array of alternate names a skill has.
     *
     * Remember, the more alternate names your skills have, the less effective 
     * alternate names become as the machine must sift through all the skill names.
     * The regular SkillName is always faster to run than an alternate name.
     * 
     * @param String[] Altnames
     * It takes an array of strings which consist of the skill's alternate names*/
    public final void setAltNames(final String[] AltNames)
    {
    	this.AltNames = AltNames;
    }
    
    /**This method is called upon when the skill is run.*/
    public abstract void use();


    @Override
    /**Returns the name of the titan :: The skill's name. It takes no parameters.*/
    public final String toString()
    {
        return this.getTitan().getName() + "::" + this.getName();
    }

}

