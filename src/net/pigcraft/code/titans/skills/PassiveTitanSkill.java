package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;

public abstract class PassiveTitanSkill// extends TitanSkill
{
	private boolean isRunning = false;
	Titan titan;
	String name;
    public PassiveTitanSkill(final String name, final Titan titan)
    {
    	this.name = name;
    	this.titan = titan;
    }
    
    public abstract void use();
    
    public abstract boolean Continue();
    
    public abstract void start();

	public boolean getRunning() {
		return isRunning;
	}
	
	public String getName()
	{
		return name;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}
}
