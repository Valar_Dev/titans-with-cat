package net.pigcraft.code.titans.skills;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.builtins.avistlo.ForceOfLight;

public class Broadcast
{
    public static void Broadcastmsg(String message, int Blockradius, Titan titan)
    {
    	ArrayList<Player> nearbyPlayers = ForceOfLight.playersDistance(titan.getController().getLocation(), Blockradius);
    	if(!message.startsWith("*"))
    		message = titan.getDisplayFormat().replace("<message>", message);
    	
    	for(Player p : nearbyPlayers)
    	{
    		p.sendMessage(message);
    	}
    }
}
