package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titans;

import org.bukkit.Bukkit;

public final class Passive
{
	
	private long BetweenRepeats;
	private long Delay = 10L;
	private PassiveTitanSkill skill;
	int task;
	
    public Passive(final long SecondsBetweenRepats, final PassiveTitanSkill skill)
	{
		this.BetweenRepeats = SecondsBetweenRepats * 20L;
		this.skill = skill;
	}
    public Passive(final long SecondsBetweenRepats, final PassiveTitanSkill skill, final long DelayBeforeFirstRun)
	{
    	this.Delay = DelayBeforeFirstRun * 20L;
		this.BetweenRepeats = SecondsBetweenRepats * 20L;
		this.skill = skill;
	}
	
	public void startSkill()
	{
		//Bukkit.getLogger().severe("asdffas");
	      task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Titans.plugin, new Runnable()
	      {
	        @Override
	        public void run()
	        {
	            skill.use();
	            if(!skill.Continue())
	            {
	            	Bukkit.getScheduler().cancelTask(task);
	            }
	        }
	      }, Delay, this.BetweenRepeats); // 10 sec delay, 1 sec cycle
	}
}
