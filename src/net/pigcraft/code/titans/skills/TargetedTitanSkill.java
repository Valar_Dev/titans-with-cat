package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;

import java.util.ArrayList;
import java.util.List;

public abstract class TargetedTitanSkill extends TitanSkill
{
    private LivingEntity target;
    private int range;

    public TargetedTitanSkill(String name, Titan titan, int range)
    {
        super(name, titan);
        this.range = range;
    }
    
    public TargetedTitanSkill(String name, Titan titan, int range, Cooldown cooldown)
    {
        super(name, titan, cooldown);
        this.range = range;
    }

    public LivingEntity getTarget()
    {
        return target;
    }

    public void setTarget(final LivingEntity entity)
    {
        this.target = entity;
    }

    public final int getRange()
    {
        return range;
    }

    public final LivingEntity getTargetedEntity()
    {
        final Player player = getTitan().getController();

        final List<Entity> entities = player.getNearbyEntities(this.range, this.range, this.range);
        final ArrayList<LivingEntity> livingEntities = new ArrayList<LivingEntity>();

        for (Entity ent : entities)
        {
            if (ent instanceof LivingEntity)
            {
                livingEntities.add((LivingEntity)ent);
            }
        }

        LivingEntity target;
        BlockIterator it;

        try
        {
            it = new BlockIterator(player, this.range);
        }
        catch (IllegalStateException ex)
        {
            Bukkit.getLogger().severe("Error occurred in TitanSkill " + this.getName() + ", " + ex.toString());
            return null;
        }

        Block block;
        Location loc;

        int blockX;
        int blockY;
        int blockZ;

        double entityX;
        double entityY;
        double entityZ;

        while (it.hasNext())
        {
            block = it.next();

            blockX = block.getX();
            blockY = block.getY();
            blockZ = block.getZ();

            if (!(block.getType() == Material.AIR))
            {
                break;
            }
            else
            {
                for (LivingEntity e : livingEntities)
                {
                    loc = e.getLocation();

                    entityX = loc.getX();
                    entityY = loc.getY();
                    entityZ = loc.getZ();

                    if ( (blockX - 0.75 <= entityX && entityX <= blockX + 1.75) && (blockZ - 0.75 <= entityZ && entityZ <= blockZ + 1.75) && (blockY - 1 <= entityY && entityY <= blockY + 2.5) )
                    {
                        target = e;
                        return target;
                    }
                }
            }


        }

        return null;

    }






}
