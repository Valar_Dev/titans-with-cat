package net.pigcraft.code.titans.skills;

import net.pigcraft.code.titans.Titan;

public abstract class ToggleableTitanSkill extends TitanSkill
{
    private boolean activated;

    public ToggleableTitanSkill(String name, Titan titan)
    {
        super(name, titan);
    }


    public abstract void onActivate();
    public abstract void onDeactivate();


    @Override
    public final void use()
    {
        this.activated = !activated;

        if (activated)
        {
            onActivate();
        }
        else
        {
            onDeactivate();
        }
    }


    public boolean isActivated()
    {
        return activated;
    }

    public void setActivated(final boolean activated)
    {
        this.activated = activated;
    }

}
