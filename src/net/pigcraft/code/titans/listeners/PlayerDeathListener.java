package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titans;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public final class PlayerDeathListener implements Listener
{

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent e)
    {
        if (Titans.plugin.isPlayerATitan(e.getEntity()))
        {
        	
        	//If interest is shown on the forums I will add custom death messages per weapon. I also will add
        	//Custom deathmessages for unknown purposes.
        	e.setDeathMessage(Titans.plugin.getTitanAssignedToPlayer(e.getEntity()).getName() + " has been slain " + 
        			"by " + e.getEntity().getKiller());
            /* This is EXACTLY what's intended, this is the code that turns a player's Titan off after they die */
            Titans.plugin.removePlayerAsTitan(e.getEntity());
        }
    }
}
