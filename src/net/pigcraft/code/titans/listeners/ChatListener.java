package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titans;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public final class ChatListener implements Listener
{

    @EventHandler(priority = EventPriority.HIGHEST)
    public final void onPlayerChat(AsyncPlayerChatEvent e)
    {

        final Player p = e.getPlayer();
        final String msg = e.getMessage();

        if (Titans.plugin.isPlayerATitan(p))
        {
            final String fmt = Titans.plugin.getTitanAssignedToPlayer(p).getDisplayFormat().replace("<message>", "%2$s");
            e.setFormat(fmt);
            e.setMessage(msg);
        }
    }

}
