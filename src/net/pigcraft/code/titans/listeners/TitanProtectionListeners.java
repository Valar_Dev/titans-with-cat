package net.pigcraft.code.titans.listeners;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

public final class TitanProtectionListeners implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageEvent(final EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) {
        	return;
        }
    	Player p = (Player) e.getEntity();
        if(Titans.plugin.isPlayerATitan(p))
        {
        	Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity());
        	for(DamageCause dc : titan.getWorldProtectionList())
        	{
        		if(e.getCause()==dc)
        		{
        			e.setCancelled(true);
        			e.setDamage(0);
        			p.setFireTicks(0);
        			return;
        		}
        	}
        }
    }
    @EventHandler
    public void onDamager(EntityDamageByEntityEvent e){
    	if(e.getDamager() instanceof Player)
    	{
    		return;
    	}
    	if(e.getEntity() instanceof Player)
    	{
            if(Titans.plugin.isPlayerATitan((Player)e.getEntity()))
            {
            	EntityType et = e.getDamager().getType();
            	if(et == EntityType.ARROW)
            	{
            		Arrow arrow = (Arrow)e.getDamager();
            		if(arrow.getShooter() instanceof Player)
            			return;
            		else
            			et = EntityType.SKELETON;
            	}
            	Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)e.getEntity());
            	for(EntityType Mob : titan.getMobProtectionList())
            	{
            		if(et == Mob)
            		{
            			e.setCancelled(true);
            			e.setDamage(0);
            			return;
            		}
            	}
            	
            }
    	}
    }
    
        @EventHandler
        public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent ev){
        	if(ev.getEntity() instanceof Player)
        	{
        		return;
        	}
        	if(ev.getTarget() instanceof Player)
        	{
                if(Titans.plugin.isPlayerATitan((Player)ev.getTarget()))
                {
                	EntityType et = ev.getEntity().getType();
                	Titan titan = Titans.plugin.getTitanAssignedToPlayer((Player)ev.getTarget());
                	for(EntityType Mob : titan.getMobProtectionList())
                	{
                		if(et == Mob)
                		{
                			ev.setTarget(null);
                			return;
                		}
                	}
                	
                }
        	}
        }

}
