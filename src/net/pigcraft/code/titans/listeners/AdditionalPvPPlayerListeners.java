package net.pigcraft.code.titans.listeners;

import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public final class AdditionalPvPPlayerListeners implements Listener
{
    @EventHandler
    public void onDamager(EntityDamageByEntityEvent e){
    	if(e.getEntity() instanceof Player)
    	{
    		Player player = (Player) e.getEntity();
    		//Reflect for Mirror Shield
    		if(player.getItemInHand().hasItemMeta())
    		{
    			List<String> lore = player.getItemInHand().getItemMeta().getLore();
    			for(int i = 0; i < lore.size(); i++)
    			{
    				String s = lore.get(i);
    				if(s.contains("Reflect:"))
    				{
    					s = s.replace("Reflect:", "");
    					if(isNumeric(s)){
    						if(e.getDamager() instanceof LivingEntity)
    						{
    							LivingEntity enemy = (LivingEntity)e.getDamager();
    							enemy.damage((Double.parseDouble(s) / 100)*e.getDamage());
    							e.setDamage(e.getDamage() - 2);
    						}
    					}
    						
    				}
    			}
    		}
    		else
    			return;
    	}
    	if(e.getDamager() instanceof Player)
    	{
    		Player player = (Player) e.getDamager();
    		//Set Damage attr
    		
    		if(player.getItemInHand().hasItemMeta())
    		{
    			List<String> lore = player.getItemInHand().getItemMeta().getLore();
    			for(int i = 0; i < lore.size(); i++)
    			{
    				String s = lore.get(i);
    				if(s.contains("SetDamage:"))
    				{
    					s = s.replace("SetDamage:", "");
    					if(isNumeric(s)){
    						e.setDamage(Double.parseDouble(s));
    					}
    						
    				}
    			}
    		}
    	}
    }
    
    public static boolean isNumeric(String str)  
    {  
      try  
      {  
        @SuppressWarnings("unused")
		double d = Double.parseDouble(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }
}