package net.pigcraft.code.titans;

import net.pigcraft.code.titans.skills.PassiveTitanSkill;
import net.pigcraft.code.titans.skills.TitanSkill;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public abstract class Titan
{

    protected final Titans plugin;

    private Player controller;
    private String name;
    private String entranceMessage;
    private String exitMessage;
    private String displayFormat;
    private String description;

    private final ArrayList<TitanSkill> skills;
    private final ArrayList<PassiveTitanSkill> Passiveskills;
    private final ArrayList<org.bukkit.entity.EntityType> mobProtectionList;
    private final ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> worldProtectionList;
    private final List<org.bukkit.inventory.ItemStack> finalDrops;
    
    /**onEntrance() Carries out everything that must be done to the controller upon
     * entering the titan state.**/
    public abstract void onEntrance();
    /**onEntrance() Carries out everything that must be done to the controller upon
     * leaving the titan state. General usage is to undo all the changes made to the player
     * upon entering.**/
    public abstract void onExit();
    
    /**This Method is called when the titan is loaded.
     * It is mainly used to register listeners and add skills to the titan.**/
    public abstract void onLoad();
    /**This method is called when the titan must be unloaded. 
     * It is generally used to clear the skills.**/
    public abstract void onUnload();

    public Titan(final Titans plugin, String name, String entranceMessage, String exitMessage, String displayFormat)
    {
        this.plugin = plugin;
        this.name = name;
        this.entranceMessage = entranceMessage;
        this.exitMessage = exitMessage;
        this.displayFormat = displayFormat;
        this.description = "";

        this.skills = new ArrayList<TitanSkill>();
        this.Passiveskills = new ArrayList<PassiveTitanSkill>();
        this.mobProtectionList = new ArrayList<org.bukkit.entity.EntityType>();
        this.worldProtectionList = new ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause>();
        this.finalDrops = new ArrayList<org.bukkit.inventory.ItemStack>();
    }
    
    /**Returns the Titan's name**/
    public final String getName()
    {
        return name;
    }
    
    /**Returns the the player who is controlling the titan**/
    public final Player getController()
    {
        return controller;
    }

    /**Returns the message that is broadcasted when a titan enters through /titan on**/
    public final String getEntranceMessage()
    {
        return entranceMessage;
    }

    /**Returns the message that is broadcasted when a titan leaves*/
    public final String getExitMessage()
    {
        return exitMessage;
    }

    public final String getDisplayFormat()
    {
        return displayFormat;
    }

    /**Returns the titans lore, or description.**/
    public final String getDescription()
    {
        return description;
    }

    /**Returns the Mob Types that the titan should not be targeted by or take damage from**/
    public final ArrayList<org.bukkit.entity.EntityType> getMobProtectionList()
    {
    	return mobProtectionList;
    }

    /**Returns the Damage Causes that the titan should take no damage for. **/
    public final ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> getWorldProtectionList()
    {
    	return worldProtectionList;
    }

    /**Add a skill to the titan's list of skills.**/
    public final void addSkill(final TitanSkill skill)
    {
        if (this.skills.contains(skill))
        {
            Bukkit.getLogger().severe("Titan " + this.getName() + "tried to register duplicate skill " + skill.getName());
        }
        else
        {
            this.skills.add(skill);
        }
    }

    /**Searches if the titan has a skill by that skill name, or alternate skill name.
     * If there is no skill by that name it returns null.**/
    public final TitanSkill getSkill(final String skillName)
    {
        for (TitanSkill skill : this.skills)
        {
            if (skill.getName().equalsIgnoreCase(skillName))
            {
                return skill;
            }
        }

        //I know I am being inefficient here, but if they use altNames how they should be used, accidents, then it'l be fine.
        for (TitanSkill skill : this.skills)
        {
        	if(skill.getAltNames() != null)
        	{
        		for(String s : skill.getAltNames())
        		{
        			if (skillName.equalsIgnoreCase(s))
        				return skill;
        		}
        	}
        }
        return null;
    }

    /**Clears all loaded skills from that titan. Generally done in the OnUnload method**/
    public final void clearSkills()
    {
        if (this.skills != null)
        {
            this.skills.clear();
        }
    }

    /**Returns an ArrayList of all the skills the Titan has
     * Does not include Passive Titan Skills**/
    public final ArrayList<TitanSkill> getSkills()
    {
        return this.skills;
    }



    /**Gets the Passive Titan Skill by the given skill name.**/
    public PassiveTitanSkill getPassiveskills(String skillName) {
        for (PassiveTitanSkill skill : this.Passiveskills)
        {
            if (skill.getName().equalsIgnoreCase(skillName))
            {
                return skill;
            }
        }
		return null;
	}
    
    /**Adds a Passive Titan Skill to the Titan**/
    public final void addPassiveSkill(final PassiveTitanSkill skill)
    {
        if (this.Passiveskills.contains(skill))
        {
            Bukkit.getLogger().severe("Titan " + this.getName() + "tried to register duplicate skill " + skill.getName());
        }
        else
        {
            this.Passiveskills.add(skill);
        }
    }
    /**Sets the Player who is controlling the Titan.**/
	public final void setController(final Player controller)
    {
        this.controller = controller;
    }

	/**Sets the name of the Titan.**/
    public final void setName(String name)
    {
        this.name = name;
    }

    /**Sets the message that is broadcasted on the titan's entrance**/
    public final void setEntranceMessage(final String entranceMessage)
    {
        this.entranceMessage = entranceMessage;
    }

    /**Sets the message that is broadcasted on titan's exit**/
    public final void setExitMessage(final String exitMessage)
    {
        this.exitMessage = exitMessage;
    }

    /**Sets the display format for everything the titan says.**/
    public final void setDisplayFormat(final String displayFormat)
    {
        this.displayFormat = displayFormat;
    }

    /**Returns a boolean for whether or not the titan has a controller.**/
    public final boolean hasController()
    {
        return this.controller != null;
    }

    /**Sets the titan's lore/description.**/
    public final void setDescription(final String description)
    {
        this.description = description;
    }

    /**Sets the protection the titan has from non-living entities. (Damage Causes)**/
    public final void SetWorldProtectionList(ArrayList<org.bukkit.event.entity.EntityDamageEvent.DamageCause> WorldProtections)
    {
    	this.worldProtectionList.addAll(WorldProtections);
    }    

    /**Sets the protection the titan has from living entities. (Entity Types)
     * This includes cancelling their targeting if entity.setTarget(null); 
     * is a valid operation for that entity.**/
    public final void SetMobProtectionList(ArrayList<org.bukkit.entity.EntityType> MobProtections)
    {
    	this.mobProtectionList.addAll(MobProtections);
    }


    /**Loads the titan from the given jar.**/
    public static Titan fromFile(final File file, final URLClassLoader loader) throws
            FileNotFoundException,
            NoSuchMethodException,
            ClassNotFoundException
    {
        Titan returnable = null;
        InputStream in = null;
        JarFile jar = null;

        try
        {
            jar = new JarFile(file);

            final JarEntry desc = jar.getJarEntry("titan.properties");
            final String[] titanProperties = new String[6];

            if (desc == null)
            {
                throw new FileNotFoundException("Jar " + jar.getName() + " does not contain a titan.properties file!");
            }

            final Properties props = new Properties();
            in = jar.getInputStream(desc);
            props.load(in);

            titanProperties[0] = props.getProperty("main");
            titanProperties[1] = props.getProperty("lore");
            titanProperties[2] = props.getProperty("enterMsg");
            titanProperties[3] = props.getProperty("exitMsg");
            titanProperties[4] = props.getProperty("name");
            titanProperties[5] = props.getProperty("chatFormat");

            final Class<? extends Titan> titan = loader.loadClass(titanProperties[0]).asSubclass(Titan.class);
            final Constructor<? extends Titan> constructor = titan.getConstructor();

            returnable = constructor.newInstance();
            returnable.setDescription(titanProperties[1]);
            returnable.setEntranceMessage(titanProperties[2]);
            returnable.setExitMessage(titanProperties[3]);
            returnable.setName(titanProperties[4]);
            returnable.setDisplayFormat(ChatColor.translateAlternateColorCodes('&', titanProperties[5]));

        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }

                if (jar != null)
                {
                    jar.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }

        return returnable;

    }

    /**Sends a message to everyone in the world. This does not use bukkit's broadcasting method.**/
    public final void broadcastMsg(final String msg)
    {
        for (final Player p : getController().getWorld().getPlayers())
        {
            p.sendMessage(msg);
        }
    }
    
    /**Returns the rewards for the person who gets the final kill on the titan**/
    public final List<ItemStack> GetFinalDrops()
    {
        return this.finalDrops;
    }
    
    public final void SetFinalDrops(List<ItemStack> drops)
    {
    	this.finalDrops.addAll(drops);
    }


}
