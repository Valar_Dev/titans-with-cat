package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class SubCommandLore implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {
        if (args.length == 1)
        {
            Titan t = Titans.plugin.getTitan(args[0]);

            if (t == null)
            {
                sender.sendMessage(ChatColor.GRAY + "Titan " + ChatColor.RED + args[0] + ChatColor.GRAY + " doesn't exist!");
                return Result.GENERIC_ERROR;
            }


            sender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + t.getName() + ChatColor.GRAY + " ====");
            sender.sendMessage("");

            if (!(t.getDescription().equals("")))
            {
                sender.sendMessage(ChatColor.AQUA + t.getDescription());
            }

            sender.sendMessage("");

            if (t.hasController())
            {
                //Possible thing to add to Titan.java, if they are online, what should the message be, or the will the player not know if the titan is online.
                sender.sendMessage(ChatColor.GRAY + t.getName() + " is currently roaming the world, watch out!");
            }
            else
            {
                //Possible thing to add to titan.java, if offline, what to say here.
                sender.sendMessage(ChatColor.GRAY + t.getName() + " is nowhere to be found, you may feel safe now.");
            }

            return Result.OKAY;
        }
        else
        {
            return Result.WRONG_USAGE;
        }

    }

    @Override
    public String getName()
    {
        return "lore";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"history", "desc", "bg"};  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getHelp()
    {
        return "Displays background information about the Titan";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan lore " + ChatColor.DARK_AQUA + "<titan name>";
    }

    @Override
    public String getPermission()
    {
        return "@description";
    }
}
