package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class SubCommandList implements ITitanSubCommand
{


    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {
        sender.sendMessage(ChatColor.GRAY + "Total loaded Titans: " + ChatColor.AQUA + Titans.getLoadedTitans().size());

        for (Titan t : Titans.getLoadedTitans())
        {
            sender.sendMessage("    " + ChatColor.AQUA + t.getName());

            if (t.hasController())
            {
                sender.sendMessage("        " + ChatColor.GRAY + "Controlled by : " + ChatColor.AQUA + t.getController().getDisplayName());
            }
        }

        return Result.OKAY;
    }

    @Override
    public String getName()
    {
        return "list";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"ls", "who"};
    }

    @Override
    public String getHelp()
    {
        return "Lists all loaded Titans and who's controlling them.";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan list";
    }

    @Override
    public String getPermission()
    {
        return "@list";
    }
}
