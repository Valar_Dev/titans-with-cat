package net.pigcraft.code.titans.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class CommandManager implements CommandExecutor
{

    protected static final String SYNTAX_ERROR = ChatColor.GRAY + "Error in command syntax! Use " + ChatColor.RED + "/titan help" + ChatColor.GRAY + " for help.";


    private static List<ITitanSubCommand> commands = null;

    public CommandManager()
    {
        if (commands == null)
        {
            commands = new ArrayList<ITitanSubCommand>(8);
        }
    }
    
    public final void loadCommands()
    {
        commands.add(new SubCommandHelp());
        commands.add(new SubCommandOn());
        commands.add(new SubCommandOff());
        commands.add(new SubCommandList());
        commands.add(new SubCommandLore());
        commands.add(new SubCommandInfo());
        commands.add(new SubCommandSkill());
        commands.add(new SubCommandSkillInfo());


        for (ITitanSubCommand cmd : commands)
        {
            Bukkit.getServer().getPluginManager().addPermission(new Permission(cmd.getPermission().replace("@", "titans.use.")));
        }

        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "[Titans] " + ChatColor.GRAY + "Loaded [" +
                ChatColor.AQUA + commands.size() + ChatColor.GRAY + "] commands!");
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
    {

        if (command.getName().equalsIgnoreCase("titan"))
        {
            if (args.length > 0)
            {
                for (ITitanSubCommand cmd : commands)
                {
                    List<String> aliases = Arrays.asList(cmd.getAliases());
                    if (cmd.getName().equalsIgnoreCase(args[0]) || aliases.contains(args[0].toLowerCase()))
                    {

                        if (commandSender.hasPermission(cmd.getPermission().replace("@", "titans.use.")) || commandSender.hasPermission("titans.*"))
                        {
                            switch (cmd.onCommand(commandSender, Arrays.copyOfRange(args, 1, args.length)))
                            {
                                case OKAY:
                                    return true;


                                case WRONG_USAGE:
                                    commandSender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY +
                                            "Error! Wrong usage of sub-command " + ChatColor.RED + args[0]);
                                    commandSender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY +
                                            " Correct usage is " + cmd.getUsage());
                                    return false;


                                case FORBIDDEN_STATE:
                                    commandSender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY +
                                            "Error! Command " + ChatColor.RED + args[0] + ChatColor.GRAY + " cannot be " +
                                            "used by non-players.");
                                    return false;


                                case GENERIC_ERROR:
                                    return false;


                                default:
                                    return false;
                            }
                        }
                        else
                        {
                            commandSender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Bukkit sad, " +
                                    "Bukkit would like to give you access to that command, but Bukkit cannot. Bukkit" +
                                    " will leak tears" + ChatColor.RED + " :'(");
                            return false;
                        }
                    }
                }

                commandSender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Error! " + ChatColor.RED +
                        args[0] + ChatColor.GRAY + " is not a Titan sub-command. Use " + ChatColor.RED + " /titan help" +
                        ChatColor.GRAY + " for help." );

                return false;
            }
            else
            {
                commandSender.sendMessage(SYNTAX_ERROR);
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    public ITitanSubCommand getSubCommand(final String name)
    {
        for (ITitanSubCommand cmd : commands)
        {
            if (cmd.getName().equalsIgnoreCase(name))
            {
                return cmd;
            }
        }

        return null;
    }

    public List<ITitanSubCommand> getSubCommands()
    {
        return commands;
    }

    public final void dispose()
    {
        if (commands != null)
        {

            for (ITitanSubCommand cmd : commands)
            {
                Bukkit.getServer().getPluginManager().removePermission(cmd.getPermission().replace("@", "titans.use"));
            }

            commands.clear();
            commands = null;
        }
    }


}
