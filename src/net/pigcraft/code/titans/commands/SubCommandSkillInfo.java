package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandSkillInfo implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {

        if (!(sender instanceof Player))
        {
            return Result.FORBIDDEN_STATE;
        }

    	if(Titans.plugin.isPlayerATitan((Player)sender))
    	{
    		if(args.length == 1)
    		{
    			TitanSkill skill = Titans.plugin.getTitanAssignedToPlayer((Player)sender).getSkill(args[0]);
    			if(skill == null)
    			{
                    sender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Error! Skill " + ChatColor.RED +
                            args[0] + ChatColor.GRAY + " is not a skill for your current Titan!");

                    return Result.WRONG_USAGE;
    			}
    			else
    			{
    				String msg = " ";
    				for(String s : skill.getAltNames())
    				{
    					msg += (s + " , ");
    				}
    				sender.sendMessage(ChatColor.GRAY + "           === " + ChatColor.AQUA + skill.getName() + ChatColor.GRAY + " === \n" +
    				ChatColor.AQUA +skill.getDescription() + ChatColor.GRAY +
    						"\nAlternate names:" + ChatColor.AQUA + msg);
    			}
    		}
    	}

    	return Result.WRONG_USAGE;
    }

    @Override
    public String getName()
    {
        return "skillInfo";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"si", "skInfo"};
    }

    @Override
    public String getHelp()
    {
        return "Displays help for a specified skill.";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan skillInfo " + ChatColor.DARK_AQUA + "<skill name>";
    }

    @Override
    public String getPermission()
    {
        return "@skill-info";
    }
}
