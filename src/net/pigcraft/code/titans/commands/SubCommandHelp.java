package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public final class SubCommandHelp implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {


        if (args.length == 1)
        {
            ITitanSubCommand cmd = Titans.getCommandManager().getSubCommand(args[0]);

            if (cmd == null)
            {
                sender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Command " + ChatColor.RED + args[0] +
                ChatColor.GRAY + " does not exist!");

                return Result.GENERIC_ERROR;
            }

            final String header = !sender.hasPermission(cmd.getPermission().replace("@", "titans.use.")) ? ChatColor.RED
                    + "(No permission)" : ChatColor.AQUA + "";

            sender.sendMessage(ChatColor.GRAY + "====" + header + " /titan " + args[0] + ChatColor.GRAY + " ====");
            sender.sendMessage("    " + cmd.getUsage());
            sender.sendMessage("    " + cmd.getHelp());

            return Result.OKAY;
        }
        else if (args.length == 0)
        {
            sender.sendMessage(ChatColor.GRAY + "==== " + ChatColor.AQUA + "Help for Titans" + ChatColor.GRAY + " ====");

            for (ITitanSubCommand cmd : Titans.getCommandManager().getSubCommands())
            {
                if (sender.hasPermission(cmd.getPermission().replace("@", "titans.use.")))
                {
                    sender.sendMessage(ChatColor.AQUA + cmd.getName() + " " + ChatColor.GRAY + Arrays.toString(cmd.getAliases()));
                    sender.sendMessage("    " + cmd.getUsage());
                    sender.sendMessage("    " + ChatColor.GRAY + cmd.getHelp());

                    sender.sendMessage("");
                }
            }

            return Result.OKAY;
        }

        return Result.WRONG_USAGE;
    }

    @Override
    public String getName()
    {
        return "help";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"?", "hlp"};
    }

    @Override
    public String getHelp()
    {
        return "Displays help for either an individual Titan command or all commands.";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan help" + ChatColor.DARK_AQUA + "[command]";
    }

    @Override
    public String getPermission()
    {
        return "@help";
    }

}
