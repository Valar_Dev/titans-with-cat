package net.pigcraft.code.titans.commands;

import org.bukkit.command.CommandSender;

public interface ITitanSubCommand
{

    Result onCommand(final CommandSender sender, final String[] args);

    String getName();

    String[] getAliases();
    String getHelp();
    String getUsage();

    String getPermission();

}
