package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandOn implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {
        if (sender instanceof Player)
        {
            if (args.length == 1)
            {
                Titan titan = Titans.plugin.getTitan(args[0]);

                if (titan == null)
                {
                    sender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[0] + ChatColor.GRAY + " is not a Titan!");
                    return Result.GENERIC_ERROR;
                }

                Titans.plugin.setPlayerAsTitan((Player)sender, titan);
                return Result.OKAY;
            }
            else
            {
                return Result.WRONG_USAGE;
            }
        }
        else
        {
            return Result.FORBIDDEN_STATE;
        }
    }

    @Override
    public String getName()
    {
        return "on";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"summon", "activate"};
    }

    @Override
    public String getHelp()
    {
        return "Transforms you into the specified Titan.";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan on " + ChatColor.DARK_AQUA + "<titan name>";
    }

    @Override
    public String getPermission()
    {
        return "@on";
    }
}
