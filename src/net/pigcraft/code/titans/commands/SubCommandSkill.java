package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;
import net.pigcraft.code.titans.skills.TitanSkill;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandSkill implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {

        if (!(sender instanceof Player))
        {
            return Result.FORBIDDEN_STATE;
        }

        if (args.length == 1)
        {

            if (args[0].equalsIgnoreCase("info"))
            {
                return Result.WRONG_USAGE;
            }

            if (Titans.plugin.isPlayerATitan((Player)sender))
            {
                TitanSkill skill = Titans.plugin.getTitanAssignedToPlayer((Player)sender).getSkill(args[0]);

                if (skill == null)
                {
                    sender.sendMessage(ChatColor.GRAY + "Error! " + ChatColor.RED + args[0] + ChatColor.GRAY +  " is " +
                            "not a skill for your current Titan!");
                    return Result.GENERIC_ERROR;
                }

                if (skill.getCooldown().done())
                {
                    skill.use();
                    skill.getCooldown().start();

                    return Result.OKAY;
                }
                else
                {
                    sender.sendMessage(ChatColor.GRAY + "Error, skill " + ChatColor.AQUA + skill.getName() +
                            ChatColor.GRAY + " is on cooldown for "
                            + ChatColor.RED + skill.getCooldown().timeLeft() +ChatColor.GRAY + " more seconds");
                }

            }
            else
            {
                sender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "You are not a Titan!");
                return Result.GENERIC_ERROR;
            }
        }
        else
        {
            if (Titans.plugin.isPlayerATitan((Player)sender))
            {
                sender.sendMessage(ChatColor.RED + "Incorrect Command Usage, You did not enter a skill name \n "
                        + ChatColor.DARK_GRAY +"Correct usage is" + ChatColor.AQUA + " /titan <use|skill> <skillName>");
                if(args.length == 0)
                {
                	for (TitanSkill skill : Titans.plugin.getTitanAssignedToPlayer((Player)sender).getSkills())
                	{
                		sender.sendMessage("\n" + ChatColor.AQUA + "/titan skill " + skill.getName());
                		sender.sendMessage("       " + ChatColor.AQUA + skill.getDescription());
                	}
                }

            	return Result.WRONG_USAGE;
            }
            else
            {
                sender.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "You are not a Titan!");
                return Result.GENERIC_ERROR;
            }
        }

        return Result.WRONG_USAGE;
    }

    @Override
    public String getName()
    {
        return "skill";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"use", "ability", "s"};
    }

    @Override
    public String getHelp()
    {
        return "Uses a specified skill for your current Titan.";
    }

    @Override
    public String getUsage()
    {
        return ChatColor.AQUA + "/titan skill " + ChatColor.DARK_AQUA + "<skill name>";
    }

    @Override
    public String getPermission()
    {
        return "@skill";
    }
}
