package net.pigcraft.code.titans.commands;

enum Result
{

    OKAY,
    WRONG_USAGE,
    GENERIC_ERROR,
    FORBIDDEN_STATE,

}
