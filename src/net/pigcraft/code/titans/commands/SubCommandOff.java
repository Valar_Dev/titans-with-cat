package net.pigcraft.code.titans.commands;

import net.pigcraft.code.titans.Titans;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class SubCommandOff implements ITitanSubCommand
{
    @Override
    public Result onCommand(CommandSender sender, String[] args)
    {
        if (!(sender instanceof Player))
        {
            return Result.FORBIDDEN_STATE;
        }

        Titans.plugin.removePlayerAsTitan((Player)sender);
        return Result.OKAY;
    }

    @Override
    public String getName()
    {
        return "off";
    }

    @Override
    public String[] getAliases()
    {
        return new String[] {"banish"};  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getHelp()
    {
        return "If you are playing as a Titan, this command returns you to your normal state.";
    }

    @Override
    public String getUsage()
    {
       return ChatColor.AQUA + "/titan off";
    }

    @Override
    public String getPermission()
    {
        return "@off";
    }

}
