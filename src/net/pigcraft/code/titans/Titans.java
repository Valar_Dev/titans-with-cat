package net.pigcraft.code.titans;

import net.pigcraft.code.titans.Titan;
import net.pigcraft.code.titans.builtins.avistlo.Avistlo;
import net.pigcraft.code.titans.builtins.ixen.IxenGix;
//import net.pigcraft.code.titans.builtins.smdp.SuperMechaDeathPika;
import net.pigcraft.code.titans.commands.*;
import net.pigcraft.code.titans.listeners.*;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public final class Titans extends JavaPlugin
{
    /**
     * Singleton instance
     */
    public static Titans plugin = null;

    private static ArrayList<Titan> loadedTitans = new ArrayList<Titan>();
    private static ArrayList<String> titanPlayers = new ArrayList<String>();

    private static File titansDir = null;

    private static  CommandManager commandManager = null;

    @Override
    public void onEnable()
    {
        plugin = this;
        titansDir = new File(this.getDataFolder() + File.separator + "ExtMods");

        if (!titansDir.exists())
        {
            if (!titansDir.mkdirs())
            {
                getLogger().severe("Critical - Failed to create Titans/ExtMods dir!");
            }
        }

        this.loadTitans();

        for (Titan t : getLoadedTitans())
        {
            t.onLoad();
        }

        commandManager = new CommandManager();

        this.getCommand("titan").setExecutor(commandManager);
        this.getServer().getPluginManager().registerEvents(new ChatListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
        this.getServer().getPluginManager().registerEvents(new TitanProtectionListeners(), this);
        this.getServer().getPluginManager().registerEvents(new AdditionalPvPPlayerListeners(), this);
        
        commandManager.loadCommands();
    }

    @Override
    public void onDisable()
    {

        for (Titan t : getLoadedTitans())
        {

            if (t.hasController())
            {
                t.getController().sendMessage(ChatColor.AQUA + "[Titans] " + ChatColor.GRAY + "You have been forced out" +
                        " of Titan form by a reload!");
                this.removePlayerAsTitan(t.getController());
            }

            t.onUnload();
        }

        loadedTitans.clear();
        titanPlayers.clear();

        commandManager.dispose();
    }


    private void loadTitans()
    {

        loadedTitans.add(new Avistlo());
        loadedTitans.add(new IxenGix());
        //loadedTitans.add(new SuperMechaDeathPika());

        if (titansDir != null)
        {
            final ArrayList<File> jars = new ArrayList<File>();

            for (String s : titansDir.list())
            {
                if (s.endsWith(".jar"))
                {
                    jars.add(new File(titansDir, s));
                }
            }

            final URL[] urls = new URL[jars.size()];

            for (int i = 0; i < jars.size(); i++)
            {
                try
                {
                    urls[i] = jars.get(i).toURI().toURL();
                }
                catch (MalformedURLException ex)
                {
                    ex.printStackTrace();
                }
            }

            final URLClassLoader classLoader = new URLClassLoader(urls, this.getClassLoader());

            for (File f : jars)
            {
                try
                {
                    getLoadedTitans().add(Titan.fromFile(f, classLoader));
                }
                catch (NoSuchMethodException ex)
                {
                    getLogger().severe("Unexpected exception while loading jar " + f.getName() +
                            ", constructor is not valid");
                    ex.printStackTrace();
                }
                catch (ClassNotFoundException ex)
                {
                    getLogger().severe("Unexpected exception while loading jar " + f.getName() +
                            ", main class not found");
                    ex.printStackTrace();
                }
                catch (FileNotFoundException ex)
                {
                    getLogger().severe("Unexpected exception while loading jar " + f.getName() +
                            ", description file not found");
                    ex.printStackTrace();
                }
            }


        }

        getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "[Titans] " + ChatColor.GRAY + "Loaded [" +
                ChatColor.AQUA + loadedTitans.size() + ChatColor.GRAY + "] Titans!");
    }

    public final boolean isPlayerATitan(final Player player)
    {
        return titanPlayers.contains(player.getName());
    }

    public final Titan getTitanAssignedToPlayer(final Player player)
    {
        for (Titan t : loadedTitans)
        {
            if (t.hasController())
            {
                if (t.getController().equals(player))
                {
                    return t;
                }
            }
        }

        return null;
    }


    public void setPlayerAsTitan(final Player player, final Titan titan)
    {
        if (!this.isPlayerATitan(player))
        {

            if (titan.hasController())
            {
                player.sendMessage(ChatColor.RED + "[Titans] " + ChatColor.GRAY + "Sorry, but that Titan is already " +
                        "controlled by " + ChatColor.RED + titan.getController().getDisplayName());
                return;
            }

            getServer().broadcastMessage(titan.getEntranceMessage());
            titan.setController(player);

            titan.getController().sendMessage(ChatColor.GRAY + "You have been transformed into the Titan " + ChatColor.AQUA + titan.getName() + "!");
            titanPlayers.add(player.getName());
            titan.onEntrance();
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are already a titan!");
        }
    }

    public void removePlayerAsTitan(final Player player)
    {
        if (this.isPlayerATitan(player))
        {

            Titan titan = getTitanAssignedToPlayer(player);

            getServer().broadcastMessage(titan.getExitMessage());
            titan.onExit();


            titanPlayers.remove(player.getName());
            titan.getController().sendMessage(ChatColor.GREEN + "You are no longer a Titan!");


            titan.setController(null);

        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are not a titan!");
        }
    }


    public Titan getTitan(final String titanName)
    {
        for (int i = 0; i < getLoadedTitans().size(); i++)
        {
            if (getLoadedTitans().get(i).getName().equalsIgnoreCase(titanName))
            {
                return getLoadedTitans().get(i);
            }
        }

        return null;
    }

    public static ArrayList<Titan> getLoadedTitans()
    {
        return loadedTitans;
    }

    public static CommandManager getCommandManager()
    {
        return commandManager;
    }







}
